//
//  MockStorageManager.swift
//  mediquo-professionals-libTests
//
//  Created by Pere Daniel Prieto on 31/12/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import mediquo_videocall_lib

class MockStorageManager {
    private var objects: [String: Data] = [:]

    func registerMock<T: Codable>(_ object: T, for key: String) {
        objects[key] = try? JSONEncoder().encode(object)
    }

    func cleanRegisteredMocks() {
        objects.removeAll()
    }
}

extension MockStorageManager: StorageManagerProtocol {
    
    func save<T>(object: T, by key: String) where T : Decodable, T : Encodable {
        objects[key] = try? JSONEncoder().encode(object)
    }
    
    func get<T>(by key: String) -> T? where T : Decodable, T : Encodable {
        guard let data = objects[key] else { return nil }
        return try? JSONDecoder().decode(T.self, from: data)
    }
    
    func delete(by key: String) {
        _ = objects.removeValue(forKey: key)
    }

    func deleteAll() {
        objects.removeAll()
    }
}
