//
//  MockRemoteManager.swift
//  mediquo-professionals-libTests
//
//  Created by Pere Daniel Prieto on 31/12/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import mediquo_videocall_lib

enum BaseError: Error {
    case random
}

enum HttpMethod: Hashable, CaseIterable {
    case get, post, put, delete
}

class MockRemoteManager {
    
    private var responses: [HttpMethod: [String: Result<Codable, Error>]] = [
        .get: [:],
        .post: [:],
        .put: [:],
        .delete: [:],
    ]

    private var ignoreParametersInKeys: Bool

    init(ignoreParametersInKeys: Bool = false) {
        self.ignoreParametersInKeys = ignoreParametersInKeys
    }

    func registerResponse(_ response: Result<Codable, Error>, forMethod method: HttpMethod, withEndpoint endpoint: String, withParameters parameters: Codable?) {
        self.responses[method]?[computeKey(using: endpoint, and: parameters)] = response
    }

    func cleanRegisteredResponses() {
        HttpMethod.allCases.forEach {
            responses[$0]?.removeAll()
        }
    }
}

extension MockRemoteManager: RemoteManagerProtocol {
    
    func post<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        
        executeClosure(completion, with: retrieveResponse(forMethod: .post, endpoint: endpoint, parameters: parameters))
    }
    
    func get<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        executeClosure(completion, with: retrieveResponse(forMethod: .get, endpoint: endpoint, parameters: parameters))
    }
    
    func put<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        
        executeClosure(completion, with: retrieveResponse(forMethod: .put, endpoint: endpoint, parameters: parameters))
    }
    
    func delete<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        executeClosure(completion, with: retrieveResponse(forMethod: .delete, endpoint: endpoint, parameters: parameters))
    }
    
}

private extension MockRemoteManager {
    private func computeKey(using endpoint: String, and parameters: Codable?) -> String {
        var key = endpoint
        let parsedParameters = parameters?.dictionary?.map { $0.key + "=" + "\($0.value)" }.sorted().joined(separator: "&")
        if !self.ignoreParametersInKeys, let parsedParameters = parsedParameters, !parsedParameters.isEmpty {
            key += "?\(parsedParameters)"
        }
        return key
    }

    private func retrieveResponse(forMethod method: HttpMethod, endpoint: String, parameters: Codable?) -> Result<Codable, Error> {
        if let response = self.responses[method]?[computeKey(using: endpoint, and: parameters)] {
            return response
        } else {
            return .failure(BaseError.random)
        }
    }

    private func executeClosure<Response: Codable>(_ closure: @escaping RemoteCompletionTypeAlias<Response>, with response: Result<Codable, Error>) {
        switch response {
        case .success(let value):
            if let value = value as? Response {
                closure(.success(value))
            } else {
                closure(.failure(BaseError.random))
            }
        case .failure(let error):
            closure(.failure(error))
        }
    }
}
