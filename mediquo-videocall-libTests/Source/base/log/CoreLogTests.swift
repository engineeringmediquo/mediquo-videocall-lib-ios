//
//  CoreLogTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class CoreLogTests: XCTestCase {

    func test_CoreLogForBusiness() {
        expect(CoreLog.business.identifier).to(equal("com.mediquo.videocall.lib"))
        expect(CoreLog.business.category).to(equal("business"))
    }

    func test_CoreLogForUI() {
        expect(CoreLog.ui.identifier).to(equal("com.mediquo.videocall.lib"))
        expect(CoreLog.ui.category).to(equal("ui"))
    }

    func test_CoreLogForRemote() {
        expect(CoreLog.remote.identifier).to(equal("com.mediquo.videocall.lib"))
        expect(CoreLog.remote.category).to(equal("remote"))
    }
}

