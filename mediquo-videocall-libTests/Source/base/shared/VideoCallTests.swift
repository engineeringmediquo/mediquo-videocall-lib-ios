//
//  VideoCallTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class VideoCallTests: XCTestCase {

    override func setUp() {
        super.setUp()
        VideoCall.initLib(to: .testing, with: "1234")
    }

    func test_checkEnvironmentSuccess() {
        expect(VideoCall.environment).to(equal(.testing))
    }

    func test_checkEnvironmentFailure() {
        expect(VideoCall.environment).toNot(equal(.develop))
    }

    func test_videoCallManagerSuccess() {
        expect(try VideoCall.getVideoCallManager()).toNot(beNil())
        expect(try VideoCall.getVideoCallManager()).to(beAnInstanceOf(VideoCallManager.self))
    }
}
