//
//  BaseViewControllerTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class BaseViewControllerTests: XCTestCase {

    func test_viewControllerSuccess() {
        VideoCall.environment = .testing
        expect(VideoCallBaseViewController().view).notTo(beNil())
    }
}
