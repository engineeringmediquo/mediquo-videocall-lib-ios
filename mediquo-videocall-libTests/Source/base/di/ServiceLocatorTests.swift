//
//  ServiceLocatorTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class ServiceLocatorTests: XCTestCase {

    func test_storageSuccessful() {
        let repository = ServiceLocator.storage
        expect(repository).to(beAnInstanceOf(StorageManager.self))
    }

    func test_remoteSuccessful() {
        let repository = ServiceLocator.remote
        expect(repository).to(beAnInstanceOf(RemoteManager.self))
    }
}
