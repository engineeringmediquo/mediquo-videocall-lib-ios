//
//  Codable+ExtensionTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class CodableExtensionTests: XCTestCase {

    private var model: CodableTestModel?

    override func setUp() {
        super.setUp()
        model = CodableTestModel(name: "Test")

    }

    func test_primitive() {
        let dict = model?.dictionary
        if let expectValue: String = dict?["name"] as? String {
            expect(dict).notTo(beNil())
            expect(expectValue).to(equal("Test"))
        }
    }

    func test_codable() {
        if let dict = model?.dictionary,
            let expctableResult = model?.name {
            do {
                let resultModel: CodableTestModel = try CodableTestModel.decode(from: dict)
                expect(expctableResult).to(equal(resultModel.name))
            } catch {
                fail(error.localizedDescription)
            }
        } else {
            fail("test model don't builded properly")
        }
    }

    func test_json() {
        if let dict = model?.dictionary,
            let expctableResult = model?.name {
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: [])
                let resultModel: CodableTestModel = try CodableTestModel.decode(data)
                expect(expctableResult).to(equal(resultModel.name))
            } catch {
                fail(error.localizedDescription)
            }
        } else {
            fail("test model don't builded properly")
        }
    }
}

private struct CodableTestModel: Codable {
    let name: String
}
