//
//  StorageManagerTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class StorageManagerTests: XCTestCase {

    private let store: StorageManagerProtocol = StorageManager()

    override func setUp() {
        super.setUp()
        store.deleteAll()
    }

    override func tearDown() {
        super.tearDown()
        store.deleteAll()
    }

    func test_saveSecuredObjectSuccess() {
        let data: ModelBuilderTest = ModelBuilderTest(name: "mediQuo")
        store.save(object: data, by: "testSaveObjectSuccess")
        if let storedData: ModelBuilderTest = store.get(by: "testSaveObjectSuccess") {
            expect(storedData).notTo(beNil())
            expect(storedData.name).to(equal("mediQuo"))
        }
    }

    func test_saveSecuredObjectFailure() {
        store.save(object: "testString", by: "testSaveObjectSuccess")
        let storedData: [ModelBuilderTest]? = store.get(by: "testSaveObjectSuccess") ?? nil
        expect(storedData).to(beNil())
    }

    func test_deleteSecuredObject() {
        let data: [ModelBuilderTest] = [ModelBuilderTest(name: "mediQuo"), ModelBuilderTest(name: "VideoCall")]
        defer {
            store.delete(by: "testSaveObjectSuccess")
            let storedData: [ModelBuilderTest]? = store.get(by: "testSaveObjectSuccess")
            expect(storedData).to(beNil())
        }
        store.save(object: data, by: "testSaveObjectSuccess")
    }

    func test_deleteSecuredAllObjects() {
        let data: [ModelBuilderTest] = [ModelBuilderTest(name: "mediQuo"), ModelBuilderTest(name: "VideoCall")]
        defer {
            store.deleteAll()
            let storedData: [ModelBuilderTest]? = store.get(by: "testSaveObjectSuccess")
            expect(storedData).to(beNil())
        }
        store.save(object: data, by: "testSaveObjectSuccess")
    }
}

private struct ModelBuilderTest: Codable {
    let name: String
}

