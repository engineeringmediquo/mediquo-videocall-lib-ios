//
//  ServiceLocatorExtensionTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class ServiceLocatorExtensionTests: XCTestCase {

    private var mainViewController: MainViewController!
    
    override func setUp() {
        mainViewController = MainViewController()
    }
    
    override  func tearDown() {
        mainViewController = nil
    }
    
    func test_environmentSuccessful() {
        let repository = ServiceLocator.environmentRepository
        expect(repository).to(beAnInstanceOf(EnvironmentRepository.self))
    }

    func test_userRepositorySuccessful() {
        let repository = ServiceLocator.userRepository
        expect(repository).to(beAnInstanceOf(UserRepository.self))
    }
    

    func test_getIncomingCallSuccess() {
        expect(ServiceLocator.getIncomingCall(userType: .patient, sessionId: "", callId: "", token: "", professional: nil)).toNot(beNil())
        expect(ServiceLocator.getIncomingCall(userType: .patient, sessionId: "", callId: "", token: "", professional: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getIncomingVideoSuccess() {
        expect(ServiceLocator.getIncomingVideo(userType: .patient, sessionId: "", callId: "", token: "", professional: nil)).toNot(beNil())
        expect(ServiceLocator.getIncomingVideo(userType: .patient, sessionId: "", callId: "", token: "", professional: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getInProgressClientCallSuccess() {
        expect(ServiceLocator.getInProgressCall(userType: .patient, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).toNot(beNil())
        expect(ServiceLocator.getInProgressCall(userType: .patient, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getInProgressProfessionalCallSuccess() {
        expect(ServiceLocator.getInProgressCall(userType: .professional, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).toNot(beNil())
        expect(ServiceLocator.getInProgressCall(userType: .professional, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getInProgressClientVideoSuccess() {
        expect(ServiceLocator.getInProgressVideo(userType: .patient, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).toNot(beNil())
        expect(ServiceLocator.getInProgressVideo(userType: .patient, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getInProgressProfessionalVideoSuccess() {
        expect(ServiceLocator.getInProgressVideo(userType: .professional, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).toNot(beNil())
        expect(ServiceLocator.getInProgressVideo(userType: .professional, sessionId: "", callId: "", token: "", customer: nil, professional: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getOutgoingCallSuccess() {
        expect(ServiceLocator.getOutgoingCall(userType: .professional, sessionId: "", callId: "", token: "", customer: nil)).toNot(beNil())
        expect(ServiceLocator.getOutgoingCall(userType: .professional, sessionId: "", callId: "", token: "", customer: nil)).to(beAnInstanceOf(MainViewController.self))
    }

    func test_getOutgoingVideoSuccess() {
        expect(ServiceLocator.getOutgoingVideo(userType: .professional, sessionId: "", callId: "", token: "", customer: nil)).toNot(beNil())
        expect(ServiceLocator.getOutgoingVideo(userType: .professional, sessionId: "", callId: "", token: "", customer: nil)).to(beAnInstanceOf(MainViewController.self))
    }
    
    func test_createPopupForAudioPermissionDenied() {
        expect(ServiceLocator.createPopupForAudioPermissionDenied(delegate: self.mainViewController)).toNot(beNil())
        expect(ServiceLocator.createPopupForAudioPermissionDenied(delegate: self.mainViewController)).to(beAnInstanceOf(PopupManagerViewController.self))
    }
    
    func test_createPopupForVideoPermissionDenied() {
        expect(ServiceLocator.createPopupForVideoPermissionDenied(delegate: self.mainViewController)).toNot(beNil())
        expect(ServiceLocator.createPopupForAudioPermissionDenied(delegate: self.mainViewController)).to(beAnInstanceOf(PopupManagerViewController.self))
    }

    func test_clientHasAudioPermissionDenied() {
        expect(ServiceLocator.clientHasAudioPermissionDenied(delegate: self.mainViewController)).toNot(beNil())
        expect(ServiceLocator.createPopupForAudioPermissionDenied(delegate: self.mainViewController)).to(beAnInstanceOf(PopupManagerViewController.self))
    }

    func test_clienttHasVideoPermissionDenied() {
        expect(ServiceLocator.clienttHasVideoPermissionDenied(delegate: self.mainViewController)).toNot(beNil())
        expect(ServiceLocator.createPopupForAudioPermissionDenied(delegate: self.mainViewController)).to(beAnInstanceOf(PopupManagerViewController.self))
    }
}
