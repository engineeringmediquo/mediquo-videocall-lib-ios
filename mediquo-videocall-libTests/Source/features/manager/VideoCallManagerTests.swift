//
//  VideoCallManagerTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class VideoCallManagerTests: XCTestCase {
    
    let mockStorage = MockStorageManager()
    let mockRemote = MockRemoteManager()

    var manager: VideoCallManagerProtocol!

    override func setUp() {
        super.setUp()
        let userRepository = UserRepository(storage: self.mockStorage)
        let repository = ServicesRepository(storage: self.mockStorage,
                                            remote: self.mockRemote, userRepository: userRepository)
        let useCase = ServicesUseCase(respository: repository)
        let presenter = ServicesPresenter(useCase: useCase)
        self.manager = VideoCallManager(presenter: presenter)
    }
    
    override func tearDown() {
        self.manager = nil
        self.mockRemote.cleanRegisteredResponses()
        super.tearDown()
    }
    
    func test_showOutgointCallSuccessful() {

        let roomId = 0
        let schema = VideoCallNotificationSchema(call: CallModel(uuid: "", roomId: roomId, type: .call))
        let response = VideoCallResponse(call: CallModel(uuid: "", roomId: roomId, type: .call),
                                         session: SessionModel(customerToken: "", professionalToken: "", sessionId: ""))
        
        self.mockRemote.registerResponse(.success(DataResponse(data: response)),
                                         forMethod: .post,
                                         withEndpoint: "/professionals/v1/rooms/\(roomId)/call",
                                         withParameters: nil)
        
        waitUntil(timeout: 15.0) { done in
            self.manager.showOutgoingCall(by: .professional,
                                     by: schema) { viewController in
                
                expect(viewController).toNot(beNil())
                done()
            }
        }
    }
    
    func test_rejectOutgointCallSuccessful() {

        let roomId = 0
        let callToken = "call-test"
        let schema = VideoCallNotificationSchema(call: CallModel(uuid: "", roomId: roomId, type: .call))
        let response = VideoCallResponse(call: CallModel(uuid: callToken, roomId: roomId, type: .call),
                                         session: SessionModel(customerToken: "", professionalToken: "", sessionId: ""))
        
        self.mockRemote.registerResponse(.success(DataResponse(data: response)),
                                         forMethod: .post,
                                         withEndpoint: "/professionals/v1/calls/\(callToken)/reject",
                                         withParameters: nil)
        
        waitUntil(timeout: 15.0) { done in
            self.manager.rejectOutgoingCall(by: .professional, by: schema) { (status) in
                expect(true).to(beTrue())
                done()
            }
        }
    }
}
