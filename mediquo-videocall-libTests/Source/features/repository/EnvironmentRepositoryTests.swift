//
//  EnvironmentRepositoryTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class EnvironmentRepositoryTests: XCTestCase {

    private let repository: EnvironmentRepositoryProtocol = ServiceLocator.environmentRepository

    override func setUp() {
        super.setUp()
        self.clear()
    }

    override func tearDown() {
        super.tearDown()
        self.clear()
    }

    func test_checkSuccess() {
        repository.set(environment: .testing)
        expect(self.repository.get()).to(equal(.testing))
    }

    func test_checkFailure() {
        repository.set(environment: .production)
        expect(self.repository.get()).notTo(equal(.testing))
    }
}

extension EnvironmentRepositoryTests {
    private func clear() {
        self.repository.clear()
    }
}
