//
//  RemoteServicesTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class RemoteServicesTests: XCTestCase {

    func test_checkApiForDevelop() {
        VideoCall.environment = .testing
        expect(RemoteServices.api).to(equal("https://chat.dev.mediquo.com"))
    }

    func test_checkApiForTesting() {
        VideoCall.environment = .testing
        expect(RemoteServices.api).to(equal("https://chat.dev.mediquo.com"))
    }

    func test_checkApiForProduction() {
        VideoCall.environment = .production
        expect(RemoteServices.api).to(equal("https://chat.api.mediquo.com"))
    }
}
