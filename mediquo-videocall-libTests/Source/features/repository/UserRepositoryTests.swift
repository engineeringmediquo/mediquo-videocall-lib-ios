//
//  UserRepositoryTests.swift
//  mediquo-videocall-libTests
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import mediquo_videocall_lib

class UserRepositoryTests: XCTestCase {

    private final let token: String = "TEST_TOKEN"

    private let repository: UserRepositoryProtocol = ServiceLocator.userRepository

    override func setUp() {
        super.setUp()
        self.repository.set(token: self.token)
    }

    override func tearDown() {
        super.tearDown()
        self.repository.clearToken()
    }

    func test_getTokenSuccessful() {
        let token = self.repository.getToken()
        expect(token).notTo(beNil())
        expect(token).to(equal(self.token))
    }
}
