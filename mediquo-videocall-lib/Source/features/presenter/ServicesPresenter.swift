//
//  ServicesPresenter.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 27/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

protocol ServicesPresenterProtocol: AnyObject {
    func call(by roomId: Int?, completion: @escaping (Result<VideoCallResponse, Error>) -> Void)
    func videoCall(by roomId: Int?, completion: @escaping (Result<VideoCallResponse, Error>) -> Void)
    func pickUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func hangUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func reject(for userType: UserType, by callId: String?, permission: PermissionRequest?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func reject(completion: @escaping (Result<VoidResponse, Error>) -> Void)
}

class ServicesPresenter: ServicesPresenterProtocol {
    private var retryCount: Int = 0
    private let useCase: ServicesUseCaseProtocol

    init(useCase: ServicesUseCaseProtocol) {
        self.useCase = useCase
    }

    func call(by roomId: Int?, completion: @escaping (Result<VideoCallResponse, Error>) -> Void) {
        if Configuration.callRetries > retryCount {
            self.retryCount += 1
            self.useCase.call(by: roomId) { result in
                if case let .success(response) = result {
                    self.retryCount = 0
                    completion(.success(response.data))
                }
                if case let .failure(error) = result {
                    CoreLog.remote.error("%@", error.localizedDescription)
                    completion(.failure(error))
                }
            }
        }
    }

    func videoCall(by roomId: Int?, completion: @escaping (Result<VideoCallResponse, Error>) -> Void) {
        if Configuration.callRetries > retryCount {
            self.retryCount += 1
            self.useCase.videoCall(by: roomId) { result in
                if case let .success(response) = result {
                    self.retryCount = 0
                    completion(.success(response.data))
                }
                if case let .failure(error) = result {
                    CoreLog.remote.error("%@", error.localizedDescription)
                    completion(.failure(error))
                }
            }
        }
    }

    func pickUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.useCase.pickUp(for: userType, by: callId, completion: completion)
    }

    func hangUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.useCase.hangUp(for: userType, by: callId, completion: completion)
    }

    func reject(for userType: UserType, by callId: String?, permission: PermissionRequest?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.useCase.reject(for: userType, by: callId, permission: permission, completion: completion)
    }

    func reject(completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.useCase.reject(completion: completion)
    }
}
