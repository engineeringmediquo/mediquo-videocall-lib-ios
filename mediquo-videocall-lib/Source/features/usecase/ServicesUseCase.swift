//
//  ServicesUseCase.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 27/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

protocol ServicesUseCaseProtocol: AnyObject {
    func call(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void)
    func videoCall(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void)
    func pickUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func hangUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func reject(for userType: UserType, by callId: String?, permission: PermissionRequest?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func reject(completion: @escaping (Result<VoidResponse, Error>) -> Void)
}

class ServicesUseCase: ServicesUseCaseProtocol {
    private var repository: ServicesRepositoryProtocol?

    func call(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void) {
        self.repository = ServiceLocator.servicesRepository
        self.repository?.call(by: roomId, completion: completion)
    }

    func videoCall(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void) {
        self.repository = ServiceLocator.servicesRepository
        self.repository?.videoCall(by: roomId, completion: completion)
    }

    func pickUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.repository = ServiceLocator.servicesRepository
        self.repository?.pickUp(for: userType, by: callId, completion: completion)
    }

    func hangUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.repository = ServiceLocator.servicesRepository
        self.repository?.hangUp(for: userType, by: callId, completion: completion)
    }

    func reject(for userType: UserType, by callId: String?, permission: PermissionRequest?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.repository = ServiceLocator.servicesRepository
        self.repository?.reject(for: userType, by: callId, permission: permission, completion: completion)
    }

    func reject(completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        self.repository = ServiceLocator.servicesRepository
        self.repository?.reject(completion: completion)
    }
}
