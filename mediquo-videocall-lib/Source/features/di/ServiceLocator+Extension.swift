//
//  ServiceLocator.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 21/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

extension ServiceLocator {

    // MARK: - Environment

    internal static var environmentRepository: EnvironmentRepositoryProtocol {
        let store: StorageManagerProtocol = self.storage
        return EnvironmentRepository(store: store)
    }

    // MARK: - User

    internal static var userRepository: UserRepositoryProtocol {
        let storage: StorageManagerProtocol = self.storage
        return UserRepository(storage: storage)
    }

    // MARK: - Services

    internal static var servicesRepository: ServicesRepositoryProtocol {
        let storage: StorageManagerProtocol = self.storage

        let baseUrl = ServiceLocator.isSdk ? RemoteServices.sdk : RemoteServices.api
        let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: baseUrl)

        let remote: RemoteManagerProtocol = RemoteManager(configuration)
        let userRepository: UserRepositoryProtocol = self.userRepository
        return ServicesRepository(storage: storage, remote: remote, userRepository: userRepository)
    }

    private static var servicesUseCase: ServicesUseCaseProtocol {
        return ServicesUseCase()
    }

    private static var servicesPresenter: ServicesPresenterProtocol {
        let useCase: ServicesUseCaseProtocol = self.servicesUseCase
        return ServicesPresenter(useCase: useCase)
    }

    // MARK: - Permissions

    private static let userPermissions: PermissionWrapperProtocol = PermissionWrapper()

    // MARK: - View Controllers

    internal class func getIncomingCall(userType: UserType,
                                        sessionId: String?,
                                        callId: String?,
                                        token: String?,
                                        professional: ProfessionalModel?) -> MainViewController? {

        guard let viewController = StoryboardScene.MainVC.storyboard.instantiateInitialViewController() as? MainViewController else { return nil }
        viewController.configuration = .incomingCall(VideoCallIncomingCallConfiguration())
        viewController.professional = professional
        viewController.callType = .call
        viewController.userType = userType
        viewController.presenter = self.servicesPresenter
        viewController.sessionId = sessionId
        viewController.callId = callId
        viewController.token = token
        viewController.userPermissions = userPermissions
        return viewController
    }

    internal class func getIncomingVideo(userType: UserType,
                                         sessionId: String?,
                                         callId: String?,
                                         token: String?,
                                         professional: ProfessionalModel?) -> MainViewController? {

        guard let viewController = StoryboardScene.MainVC.storyboard.instantiateInitialViewController() as? MainViewController else { return nil }
        viewController.configuration = .incomingVideo(VideoCallIncomingVideoConfiguration())
        viewController.professional = professional
        viewController.callType = .video
        viewController.userType = userType
        viewController.presenter = self.servicesPresenter
        viewController.sessionId = sessionId
        viewController.callId = callId
        viewController.token = token
        viewController.userPermissions = userPermissions
        return viewController
    }

    internal class func getInProgressCall(userType: UserType, sessionId: String?, callId: String?, token: String?, customer: CustomerModel?,
                                          professional: ProfessionalModel?) -> MainViewController? {
        guard let viewController = StoryboardScene.MainVC.storyboard.instantiateInitialViewController() as? MainViewController else { return nil }
        viewController.configuration = .inProgressCall(VideoCallInProgressCallConfiguration())
        viewController.customer = customer
        viewController.professional = professional
        viewController.callType = .call
        viewController.userType = userType
        viewController.presenter = self.servicesPresenter
        viewController.sessionId = sessionId
        viewController.callId = callId
        viewController.token = token
        viewController.userPermissions = userPermissions
        return viewController
    }

    internal class func getInProgressVideo(userType: UserType, sessionId: String?, callId: String?, token: String?, customer: CustomerModel?,
                                           professional: ProfessionalModel?) -> MainViewController? {
        guard let viewController = StoryboardScene.MainVC.storyboard.instantiateInitialViewController() as? MainViewController else { return nil }
        viewController.configuration = .inProgressVideo(VideoCallInProgressVideoConfiguration())
        viewController.customer = customer
        viewController.professional = professional
        viewController.callType = .video
        viewController.userType = userType
        viewController.presenter = self.servicesPresenter
        viewController.sessionId = sessionId
        viewController.callId = callId
        viewController.token = token
        viewController.userPermissions = userPermissions
        return viewController
    }

    internal class func getOutgoingCall(userType: UserType,
                                        sessionId: String?,
                                        callId: String?,
                                        token: String?,
                                        customer: CustomerModel?) -> MainViewController? {

        guard let viewController = StoryboardScene.MainVC.storyboard.instantiateInitialViewController() as? MainViewController else { return nil }
        viewController.configuration = .outgoingCall(VideoCallOutgoingCallConfiguration())
        viewController.customer = customer
        viewController.callType = .call
        viewController.userType = userType
        viewController.presenter = self.servicesPresenter
        viewController.sessionId = sessionId
        viewController.callId = callId
        viewController.token = token
        viewController.userPermissions = userPermissions
        return viewController
    }

    internal class func getOutgoingVideo(userType: UserType,
                                         sessionId: String?,
                                         callId: String?,
                                         token: String?,
                                         customer: CustomerModel?) -> MainViewController? {

        guard let viewController = StoryboardScene.MainVC.storyboard.instantiateInitialViewController() as? MainViewController else { return nil }
        viewController.configuration = .outgoingVideo(VideoCallOutgoingVideoConfiguration())
        viewController.customer = customer
        viewController.callType = .video
        viewController.userType = userType
        viewController.presenter = self.servicesPresenter
        viewController.sessionId = sessionId
        viewController.callId = callId
        viewController.token = token
        viewController.userPermissions = userPermissions
        return viewController
    }

    internal class func getCreateReport(model: CreateReportModel?) -> CreateReportViewController? {
        guard let viewController = StoryboardScene.CreateReportVC.storyboard.instantiateInitialViewController() as? CreateReportViewController else { return nil }
        viewController.model = model
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    // MARK: - Popup View Controllers

    internal class func createPopupForAudioPermissionDenied(delegate: PopupManagerViewControllerProtocol) -> PopupManagerViewController? {
        guard let viewController = StoryboardScene.PopupVC.storyboard.instantiateInitialViewController() as? PopupManagerViewController else { return nil }
        let popup = PopupForAudioConfiguration(popupImage: Asset.imgCallOff.image,
                                               popupTitle: ServiceLocator.localizable.permissionAudioTitle,
                                               popupSubtitle: ServiceLocator.localizable.permissionAudioSubTitle,
                                               activeButtonTitle: ServiceLocator.localizable.permissionAudioButton)
        viewController.configuration = .popupForAudioPermissionDenied(popup)
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    internal class func createPopupForVideoPermissionDenied(delegate: PopupManagerViewControllerProtocol) -> PopupManagerViewController? {
        guard let viewController = StoryboardScene.PopupVC.storyboard.instantiateInitialViewController() as? PopupManagerViewController else { return nil }
        let popup = PopupForVideoConfiguration(popupImage: Asset.imgVideocallOff.image,
                                               popupTitle: ServiceLocator.localizable.permissionVideoTitle,
                                               popupSubtitle: ServiceLocator.localizable.permissionVideoSubTitle,
                                               activeButtonTitle: ServiceLocator.localizable.permissionVideoButton)
        viewController.configuration = .popupForVideoPermissionDenied(popup)
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    // MARK: - Popups Profesional

    internal class func clientHasAudioPermissionDenied(delegate: PopupManagerViewControllerProtocol) -> PopupManagerViewController? {
        guard let viewController = StoryboardScene.PopupVC.storyboard.instantiateInitialViewController() as? PopupManagerViewController else { return nil }
        let popup = PopupClientHasAudioPermissionDeniedConfiguration(popupImage: Asset.imgCallOff.image,
                                                                     popupTitle: ServiceLocator.localizable.permissionAudioDeniedTitle,
                                                                     popupSubtitle: ServiceLocator.localizable.permissionAudioDeniedSubTitle,
                                                                     activeButtonTitle: ServiceLocator.localizable.permissionAudioDeniedButton)
        viewController.configuration = .popupClientHasAudioPermissionDenied(popup)
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    internal class func clienttHasVideoPermissionDenied(delegate: PopupManagerViewControllerProtocol) -> PopupManagerViewController? {
        guard let viewController = StoryboardScene.PopupVC.storyboard.instantiateInitialViewController() as? PopupManagerViewController else { return nil }
        let popup = PopupClientHasVideoPermissionDeniedConfiguration(popupImage: Asset.imgVideocallOff.image,
                                                                     popupTitle: ServiceLocator.localizable.permissionVideoDeniedTitle,
                                                                     popupSubtitle: ServiceLocator.localizable.permissionVideoDeniedSubTitle,
                                                                     activeButtonTitle: ServiceLocator.localizable.permissionVideoDeniedButton)
        viewController.configuration = .popupClientHasVideoPermissionDenied(popup)
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    // MARK: - Managers

    internal class func videoCallManager() -> VideoCallManagerProtocol {
        return VideoCallManager(presenter: self.servicesPresenter)
    }

    // MARK: - Busy popup

    internal class func getBusyPopup() -> BusyPopupViewController? {
        guard let viewController = StoryboardScene.BusyVC.storyboard.instantiateInitialViewController() as? BusyPopupViewController else { return nil }
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    // MARK: - Rating view controller

    internal class func getRatingCall(callId: String?, avatar: String?, name: String?, time: String?) -> RatingViewController? {
        guard let viewController = StoryboardScene.RatingVC.storyboard.instantiateInitialViewController() as? RatingViewController else { return nil }
        viewController.callType = .call
        viewController.callId = callId
        viewController.avatar = avatar
        viewController.name = name
        viewController.time = time
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }

    internal class func getRatingVideo(callId: String?, avatar: String?, name: String?, time: String?) -> RatingViewController? {
        guard let viewController = StoryboardScene.RatingVC.storyboard.instantiateInitialViewController() as? RatingViewController else { return nil }
        viewController.callType = .video
        viewController.callId = callId
        viewController.avatar = avatar
        viewController.name = name
        viewController.time = time
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overFullScreen
        return viewController
    }
}
