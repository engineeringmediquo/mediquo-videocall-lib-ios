//
//  ServicesRepository.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 27/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

protocol ServicesRepositoryProtocol: AnyObject {
    func call(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void)
    func videoCall(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void)
    func pickUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func hangUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func reject(for userType: UserType, by callId: String?, permission: PermissionRequest?, completion: @escaping (Result<VoidResponse, Error>) -> Void)
    func reject(completion: @escaping (Result<VoidResponse, Error>) -> Void)
}

class ServicesRepository: ServicesRepositoryProtocol {
    private let remote: RemoteManagerProtocol
    private let storage: StorageManagerProtocol
    private let userRepository: UserRepositoryProtocol

    private enum ClientEndpoint {
        static let pickUp = "/api/v1/calls/{call}/pick-up"
        static let hangUp = "/api/v1/calls/{call}/hang-up"
        static let reject = "/api/v1/calls/{call}/reject"
    }

    private enum ClientSdkEndpoint {
        static let pickUp = "/v1/calls/{call}/pick-up"
        static let hangUp = "/v1/calls/{call}/hang-up"
        static let reject = "/v1/calls/{call}/reject"
    }

    private enum ProfessionalEndpoint {
        static let call = "/professionals/v1/rooms/{room}/call"
        static let videoCall = "/professionals/v1/rooms/{room}/video-call"
        static let pickUp = "/professionals/v1/calls/{call}/pick-up"
        static let hangUp = "/professionals/v1/calls/{call}/hang-up"
        static let reject = "/professionals/v1/calls/{call}/reject"
        static let rejectAll = "/professionals/v1/calls/reject"
    }

    init(storage: StorageManagerProtocol, remote: RemoteManagerProtocol, userRepository: UserRepositoryProtocol) {
        self.storage = storage
        self.remote = remote
        self.userRepository = userRepository
    }

    func call(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void) {
        guard let roomId = roomId else { return }
        let endpoint = ProfessionalEndpoint.call.replacingOccurrences(of: "{room}", with: roomId.description)
        let token: String? = self.userRepository.getToken()
        self.remote.post(endpoint, parameters: nil, token: token, completion: completion)
    }

    func videoCall(by roomId: Int?, completion: @escaping (Result<DataResponse<VideoCallResponse>, Error>) -> Void) {
        guard let roomId = roomId else { return }
        let endpoint = ProfessionalEndpoint.videoCall.replacingOccurrences(of: "{room}", with: roomId.description)
        let token: String? = self.userRepository.getToken()
        self.remote.post(endpoint, parameters: nil, token: token, completion: completion)
    }

    func pickUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        guard let callId = callId else { return }
        var endpoint: String
        switch userType {
        case .patient:
            let endpointType = ServiceLocator.isSdk ? ClientSdkEndpoint.pickUp : ClientEndpoint.pickUp
            endpoint = endpointType.replacingOccurrences(of: "{call}", with: callId)
        case .professional:
            endpoint = ProfessionalEndpoint.pickUp.replacingOccurrences(of: "{call}", with: callId)
        }
        let token: String? = self.userRepository.getToken()
        self.remote.post(endpoint, parameters: nil, token: token, completion: completion)
    }

    func hangUp(for userType: UserType, by callId: String?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        guard let callId = callId else { return }
        var endpoint: String
        switch userType {
        case .patient:
            let endpointType = ServiceLocator.isSdk ? ClientSdkEndpoint.hangUp : ClientEndpoint.hangUp
            endpoint = endpointType.replacingOccurrences(of: "{call}", with: callId)
        case .professional:
            endpoint = ProfessionalEndpoint.hangUp.replacingOccurrences(of: "{call}", with: callId)
        }
        let token: String? = self.userRepository.getToken()
        self.remote.post(endpoint, parameters: nil, token: token, completion: completion)
    }

    func reject(for userType: UserType, by callId: String?, permission: PermissionRequest?, completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        guard let callId = callId else { return }
        var endpoint: String
        switch userType {
        case .patient:
            let endpointType = ServiceLocator.isSdk ? ClientSdkEndpoint.reject : ClientEndpoint.reject
            endpoint = endpointType.replacingOccurrences(of: "{call}", with: callId)
        case .professional:
            endpoint = ProfessionalEndpoint.reject.replacingOccurrences(of: "{call}", with: callId)
        }
        let token: String? = self.userRepository.getToken()
        self.remote.post(endpoint, parameters: permission, token: token, completion: completion)
    }

    func reject(completion: @escaping (Result<VoidResponse, Error>) -> Void) {
        let endpoint = ProfessionalEndpoint.rejectAll
        let token: String? = self.userRepository.getToken()
        self.remote.post(endpoint, parameters: nil, token: token, completion: completion)
    }
}
