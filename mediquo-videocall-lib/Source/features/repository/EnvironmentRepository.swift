//
//  EnvironmentRepository.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

protocol EnvironmentRepositoryProtocol: AnyObject {
    func set(environment: Environment)
    func get() -> Environment
    func clear()
}

class EnvironmentRepository: EnvironmentRepositoryProtocol {
    private var store: StorageManagerProtocol?
    private let key: String = String(describing: EnvironmentRepository.self)

    init(store: StorageManagerProtocol) {
        self.store = store
    }

    func set(environment: Environment) {
        self.store?.save(object: EnvironmentModel(environment: environment.rawValue), by: self.key)
    }

    func get() -> Environment {
        #if TEST
            return .testing
        #else
            guard let model: EnvironmentModel = self.store?.get(by: key),
                let environment: Environment = Environment(rawValue: model.environment) else {
                return .develop
            }
            return environment
        #endif
    }

    func clear() {
        self.store?.delete(by: self.key)
    }
}

struct EnvironmentModel: Codable, Equatable {
    var environment: String
}
