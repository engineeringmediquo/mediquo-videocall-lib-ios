//
//  UserRepository.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 27/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

protocol UserRepositoryProtocol: AnyObject {
    func set(token: String)
    func getToken() -> String?
    func clearToken()
}

class UserRepository: UserRepositoryProtocol {
    private let keyUserToken: String = String(describing: UserRepository.self)
    private let storage: StorageManagerProtocol

    init(storage: StorageManagerProtocol) {
        self.storage = storage
    }

    func set(token: String) {
        self.storage.save(object: UserToken(token: token), by: self.keyUserToken)
    }

    func getToken() -> String? {
        guard let userToken: UserToken? = self.storage.get(by: self.keyUserToken) else { return nil }
        return userToken?.token
    }

    func clearToken() {
        self.storage.delete(by: self.keyUserToken)
    }
}

private struct UserToken: Codable {
    let token: String
}
