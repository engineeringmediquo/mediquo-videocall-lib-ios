//
//  RemoteServices.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

class RemoteServices {
    private enum Api: String {
        case develop = "https://chat-dev.mediquo.com"
        case production = "https://chat.api.mediquo.com"
    }
    private enum Sdk: String {
        case develop = "https://sdk.dev.mediquo.com"
        case production = "https://sdk.mediquo.com"
    }
}

extension RemoteServices {
    static var api: String {
        switch ServiceLocator.environmentRepository.get() {
        case .develop, .testing:
            return Api.develop.rawValue
        case .production:
            return Api.production.rawValue
        }
    }

    static var sdk: String {
        switch ServiceLocator.environmentRepository.get() {
        case .develop, .testing:
            return Sdk.develop.rawValue
        case .production:
            return Sdk.production.rawValue
        }
    }
}
