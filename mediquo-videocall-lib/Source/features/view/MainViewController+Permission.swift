//
//  MainViewController+Permission.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 11/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import PromiseKit

// MARK: - Promise Call Permission

extension MainViewController {
    internal func initPromiseForAudioPermission() {
        _ = firstly {
            self.askUserForAudioPermissions()
        }.done { _ in
            self.doPublish()
        }.catch { _ in
            guard let viewController = ServiceLocator.createPopupForAudioPermissionDenied(delegate: self) else { return }
            self.present(viewController, animated: true)
        }
    }

    private func askUserForAudioPermissions() -> Promise<Bool> {
        return Promise<Bool> { seal in
            self.notifyPermissionAudioRequest()
            userPermissions?.checkMicrophoneAvailable { audioPermission in
                self.audioPermission = audioPermission
                if audioPermission {
                    self.notifyPermissionAudioAccept()
                }
                audioPermission ? seal.fulfill(true) : seal.reject(PermissionErrorType.audioPermissionDenied)
            }
        }
    }
}

// MARK: - Promise Videocall Permission

extension MainViewController {
    internal func initPromiseForVideoPermission() {
        _ = firstly {
            self.askUserForVideoPermissions()
        }.then { _ in
            self.askUserForAudioPermissions()
        }.done { _ in
            self.doPublish()
        }.catch { _ in
            guard let viewController = ServiceLocator.createPopupForVideoPermissionDenied(delegate: self) else { return }
            self.present(viewController, animated: true)
        }
    }

    private func askUserForVideoPermissions() -> Promise<Bool> {
        return Promise<Bool> { seal in
            self.notifyPermissionVideoRequest()
            userPermissions?.checkVideoCameraAvailable { videoPermission in
                self.videoPermission = videoPermission
                if videoPermission {
                    self.notifyPermissionVideoAccept()
                }
                videoPermission ? seal.fulfill(true) : seal.reject(PermissionErrorType.videoPermissionDenied)
            }
        }
    }
}

// MARK: - PopupManagerViewControllerProtocol

extension MainViewController: PopupManagerViewControllerProtocol {
    func closeModal() {
        self.dismiss(animated: true) {
            self.rejectCall()
        }
    }

    func performAction() {
        if self.isClientPermissionDeniedPopup {
            self.rejectCall()
        } else {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
                self.closeModal()
            }
        }
    }
}
