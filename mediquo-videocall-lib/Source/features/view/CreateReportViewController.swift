//
//  CreateReportViewController.swift
//  mediquo-videocall-lib
//
//  Created by Chus Clua on 03/07/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit
import Foundation

public class CreateReportViewController: VideoCallBaseViewController {

    @IBOutlet private var contactView: UIImageView! {
        didSet {
            self.contactView.backgroundColor = ColorName.lightPurple.color
            self.contactView.circularShape()
        }
    }

    @IBOutlet private var contactInitialLabel: UILabel! {
        didSet {
            self.contactInitialLabel.textColor = ColorName.indigoBlue.color
            self.contactInitialLabel.font = FontFamily.GothamRounded.medium.font(size: 32)
        }
    }

    @IBOutlet private var callLabel: UILabel! {
        didSet {
            self.callLabel.text = ServiceLocator.localizable.endedVideoCall
            self.callLabel.textColor = .white
            self.callLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 12)
        }
    }

    @IBOutlet private var timerLabel: UILabel! {
        didSet {
            self.timerLabel.textColor = .white
            self.timerLabel.cornerRounded()
            self.timerLabel.backgroundColor = .black
            self.timerLabel.font = UIFont(name: FontFamily.GothamRounded.book.name, size: 14)
            self.timerLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }

    @IBOutlet private var messageLabel: UILabel! {
        didSet {
            self.messageLabel.text = ServiceLocator.localizable.createReportMessage
            self.messageLabel.textColor = .white
            self.messageLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 18)
        }
    }

    @IBOutlet private var exitButton: UIButton! {
        didSet {
            self.exitButton.backgroundColor = ColorName.white.color
            self.exitButton.layer.cornerRadius = 10
            self.exitButton.layer.borderWidth = 1
            self.exitButton.layer.borderColor = ColorName.indigoBlue.color.cgColor
            self.exitButton.setTitleColor(ColorName.indigoBlue.color, for: .normal)

            self.exitButton.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 18)
            self.exitButton.setTitle(ServiceLocator.localizable.createReportExitButton, for: .normal)
            self.exitButton.contentHorizontalAlignment = .center
            self.exitButton.contentVerticalAlignment = .center
        }
    }

    @IBOutlet private var createButton: UIButton! {
        didSet {
            self.createButton.setTitleColor(ColorName.white.color, for: .normal)
            self.createButton.layer.cornerRadius = 10
            self.createButton.backgroundColor = ColorName.indigoBlue.color
            self.createButton.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 18)
            self.createButton.setTitle(ServiceLocator.localizable.createReportCreateButton, for: .normal)
            self.createButton.contentHorizontalAlignment = .center
            self.createButton.contentVerticalAlignment = .center
        }
    }

    @IBAction func exit(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func create(_ sender: Any) {
        self.dismiss(animated: true) {
            if let uuid = self.model?.uuid {
                NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Report.create, object: uuid)
            }
        }
    }

    var model: CreateReportModel?

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.initBackground()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.bindInitial()
        self.bindTime()
    }

    private func bindInitial() {
        guard let name = self.model?.name else { return }
        self.contactInitialLabel.text = name.first?.uppercased() ?? ""
    }

    private func bindTime() {
        guard let time = self.model?.time else { return }
        self.timerLabel.text = time
    }

    private func initBackground() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
    }

}
