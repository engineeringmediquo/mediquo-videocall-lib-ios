//
//  MainViewController.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit
import OpenTok

public protocol MediquoSessionDelegate {
    /// Notify when the stream was ended
    func streamDestroyed()
    /// Notify when the stream was rated
    func streamRated()
}

public class MainViewController: VideoCallBaseViewController {
    @IBOutlet internal var fullScreenCameraView: UIView! {
        didSet {
            self.fullScreenCameraView.backgroundColor = ColorName.background.color
            self.backgroundHeader()
        }
    }
    @IBOutlet internal var littleScreenCameraView: UIView! {
        didSet {
            self.littleScreenCameraView.cornerRounded()
        }
    }
    @IBOutlet internal var leftLittleScreenImage: UIImageView! {
        didSet {
            self.leftLittleScreenImage.isHidden = true
            self.configBackgroundView(with: leftLittleScreenImage, and: Asset.cameraOnIcon.image)
        }
    }
    @IBOutlet internal var centerLittleScreenImage: UIImageView! {
        didSet {
            self.centerLittleScreenImage.isHidden = true
            self.configBackgroundView(with: centerLittleScreenImage, and: Asset.cameraOnIcon.image)
        }
    }
    @IBOutlet internal var rightLittleScreenImage: UIImageView! {
        didSet {
            self.rightLittleScreenImage.isHidden = true
        }
    }
    @IBOutlet internal var toastCentralInfoView: UIView! {
        didSet {
            self.toastCentralInfoView.isHidden = true
            self.configBackgroundView(with: toastCentralInfoView, alpha: 0.5)
        }
    }
    @IBOutlet internal var leftToastCentralImage: UIImageView! {
        didSet {
            self.leftToastCentralImage.isHidden = true
            self.leftToastCentralImage.tintColor = .white
        }
    }
    @IBOutlet internal var rightToastCentralImage: UIImageView! {
        didSet {
            self.rightToastCentralImage.isHidden = true
            self.rightToastCentralImage.tintColor = .white
        }
    }
    @IBOutlet internal var infoStatusToastDevicesOnCallLabel: UILabel! {
        didSet {
            self.infoStatusToastDevicesOnCallLabel.isHidden = true
            self.infoStatusToastDevicesOnCallLabel.textColor = .white
            self.infoStatusToastDevicesOnCallLabel.font = UIFont(name: FontFamily.GothamRounded.book.name, size: 12)
        }
    }
    @IBOutlet internal var timeTotalOnCallLabel: UILabel! {
        didSet {
            self.timeTotalOnCallLabel.textColor = .white
            self.configBackgroundView(with: self.timeTotalOnCallLabel, alpha: 0.5)
            self.timeTotalOnCallLabel.isHidden = true
            self.timeTotalOnCallLabel.text = "00:00"
        }
    }
    @IBOutlet internal var nameDoctorLabel: UILabel! {
        didSet {
            self.nameDoctorLabel.textColor = .white
            self.configBackgroundView(with: self.nameDoctorLabel, alpha: 0.5)
            self.nameDoctorLabel.font = FontFamily.GothamRounded.book.font(size: 14)
        }
    }
    @IBOutlet internal var firstLetterPatientView: UILabel! {
        didSet {
            self.firstLetterPatientView.font = FontFamily.GothamRounded.medium.font(size: 32)
            self.firstLetterPatientView.textColor = ColorName.purple.color
        }
    }
    @IBOutlet internal var professionalImageView: UIImageView! {
        didSet {
            self.professionalImageView.backgroundColor = .white
            self.professionalImageView.circularShape()
        }
    }
    @IBOutlet internal var namePatientOrProfessionalLabel: UILabel! {
        didSet {
            self.namePatientOrProfessionalLabel.textColor = .white
            self.configBackgroundView(with: self.namePatientOrProfessionalLabel, alpha: 0.0)
            self.namePatientOrProfessionalLabel.font = FontFamily.GothamRounded.medium.font(size: 18)
        }
    }
    @IBOutlet internal var statusCallLabel: UILabel! {
        didSet {
            self.statusCallLabel.textColor = .white
            self.configBackgroundView(with: self.statusCallLabel, alpha: 0.0)
            self.statusCallLabel.font = FontFamily.GothamRounded.medium.font(size: 12)
        }
    }
    @IBOutlet internal var microStackButton: UIButton! {
        didSet {
            self.configBackgroundButton(with: self.microStackButton, and: Asset.microphoneOnIconStack.image, and: ColorName.black.color, with: 0.5)
        }
    }
    @IBOutlet internal var cameraButton: UIButton! {
        didSet {
            self.configBackgroundButton(with: self.cameraButton, and: Asset.cameraOnIconStack.image, and: ColorName.black.color, with: 0.5)
        }
    }

    @IBOutlet internal var swapCameraOrSpeakerButton: UIButton! {
        didSet {
            let icon: UIImage = self.callType == .call ? Asset.speakerOffIconStack.image : Asset.reverseCameraOffIconStack.image
            self.configBackgroundButton(with: self.swapCameraOrSpeakerButton, and: icon, and: ColorName.black.color, with: 0.5)
        }
    }

    @IBOutlet internal var endOrRejectCallButton: UIButton! {
        didSet {
            self.configBackgroundButton(with: self.endOrRejectCallButton, and: Asset.phoneOnIcon.image, and: ColorName.redRejected.color, with: 1)
        }
    }
    @IBOutlet internal var rejectCallButton: UIButton! {
        didSet {
            self.configBackgroundButton(with: self.rejectCallButton, and: Asset.rejectCallOrVideoCall.image, and: ColorName.redRejected.color, with: 1)
        }
    }
    @IBOutlet internal var acceptCallButton: UIButton! {
        didSet {
            switch self.callType {
            case .call:
                self.configBackgroundButton(with: self.acceptCallButton, and: Asset.acceptCall.image, and: ColorName.greenAccepted.color, with: 1)
            case .video:
                self.configBackgroundButton(with: self.acceptCallButton, and: Asset.acceptCallOrVideoCall.image, and: ColorName.greenAccepted.color, with: 1)
            default:
                self.configBackgroundButton(with: self.acceptCallButton, and: Asset.acceptCall.image, and: ColorName.greenAccepted.color, with: 1)
            }
        }
    }
    @IBOutlet internal var rejectCallOrVideoCallLabel: UILabel! {
        didSet {
            self.configPatientLabel(with: self.rejectCallOrVideoCallLabel, stringLetter: ServiceLocator.localizable.refuse, sizeLetter: 14,
                                    and: FontFamily.GothamRounded.medium.name, colorLetterText: .white)
        }
    }
    @IBOutlet internal var acceptCallOrVideoCallLabel: UILabel! {
        didSet {
            self.configPatientLabel(with: self.acceptCallOrVideoCallLabel, stringLetter: ServiceLocator.localizable.accept, sizeLetter: 14,
                                    and: FontFamily.GothamRounded.medium.name, colorLetterText: .white)
        }
    }

    internal var configuration: VideoCallConfigurationType? {
        didSet {
            if self.isViewLoaded {
                self.loadScene()
            }
        }
    }

    internal var configScreen: VideoCallConfiguration? {
        didSet {
            self.manageScene()
            self.refreshScene()
        }
    }

    internal var isAciveMic = true

    @IBOutlet internal var backgroundHeaderView: UIView!

    var customer: CustomerModel?
    var professional: ProfessionalModel?
    var callType: CallType?
    var userType: UserType?
    var presenter: ServicesPresenterProtocol?
    var sessionId: String?
    var callId: String?
    var token: String?
    var userPermissions: PermissionWrapperProtocol?
    var pollingTimeInterval: Double = 5
    var enableReport: Bool?

    internal var ringSoundTimer = Timer()
    internal var pollingCheckStatusTimer: Timer?

    internal var subscriber: OTSubscriber?
    internal var publisherView: UIView?
    internal var counter: Int = 1

    internal let dateFormatter = DateFormatter()

    internal var publishVideoAudio: Bool = true
    internal var mediaController: MediaControllerProtocol?

    internal var audioPermission: Bool = false
    internal var videoPermission: Bool = false

    public var isClientPermissionDeniedPopup = false

    internal lazy var session: OTSession? = { [weak self] () -> OTSession? in
        guard let self = self, let sessionId = self.sessionId else { return nil }
        return OTSession(apiKey: Configuration.apiKey, sessionId: sessionId, delegate: self)
    }()

    internal lazy var publisher: OTPublisher? = { [weak self] () -> OTPublisher? in
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        settings.videoTrack = (self?.callType == .video)
        return OTPublisher(delegate: self, settings: settings)
    }()

    public var mediquoSessionDelegate: MediquoSessionDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.disableAutoLockScreen()
        self.loadScene()
        self.pollingSubscriber()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.notifyCallView()
        UIDevice.current.isProximityMonitoringEnabled = true
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.enableAutoLockScreen()
        self.ringSoundTimer.invalidate()
        self.pollingCheckStatusTimer?.invalidate()
        self.mediaController?.stopSound()
        UIDevice.current.isProximityMonitoringEnabled = false
    }
}

// MARK: Control scenarios

extension MainViewController {
    private func loadScene() {
        switch self.configuration {
        case .incomingCall(let configScreen):
            self.configScreen = configScreen
        case .inProgressCall(let configScreen):
            self.configScreen = configScreen
        case .outgoingCall(let configScreen):
            self.configScreen = configScreen
        case .incomingVideo(let configScreen):
            self.configScreen = configScreen
        case .inProgressVideo(let configScreen):
            self.configScreen = configScreen
        case .outgoingVideo(let configScreen):
            self.configScreen = configScreen
        default:
            break
        }
    }

    private func manageScene() {
        switch self.configuration {
        case .incomingCall, .incomingVideo:
            self.mediaController = MediaController(soundType: .ring)
            self.mediaController?.playSound()
        case .inProgressCall, .inProgressVideo:
            VideoCall.isInProgress = true
            if self.userType == .patient {
                self.connect()
            }
            self.mediaController?.stopSound()
        case .outgoingCall, .outgoingVideo:
            self.mediaController = MediaController(soundType: .wait)
            self.mediaController?.playSound()
            self.connect()
        default:
            self.mediaController?.stopSound()
        }
    }
}

// MARK: Acttion Buttons

public extension MainViewController {
    @IBAction private func enableAndDissableMicroStackAction(_ sender: Any) {
        if let publisher = self.publisher {
            self.notifyToggleAudio()
            publisher.publishAudio = !publisher.publishAudio
            if publisher.publishAudio {
                self.disableButton(on: microStackButton, with: Asset.microphoneOnIconStack.image)
            } else {
                self.enableButton(on: microStackButton, with: Asset.microphoneOffIconStack.image, with: ColorName.redRejected.color)
                switch self.callType {
                case .call:
                    self.configToastCentralPublisher(microStackButton, leftToastImaged: nil, rightToastImage: Asset.toastSatusMicrophoneOffIcon.image, infoLabel: ServiceLocator.localizable.microphoneDesactivated) // swiftlint:disable:this line_length
                case .video:
                    if !publisher.publishVideo && !publisher.publishAudio {
                        self.configToastCentralPublisher(microStackButton, leftToastImaged: Asset.toastSatusMicrophoneOffIcon.image, rightToastImage: Asset.toastSatusCameraOffIcon.image, infoLabel: ServiceLocator.localizable.cameraAndMicrophoneDeactivated) // swiftlint:disable:this line_length
                    } else {
                        self.configToastCentralPublisher(microStackButton, leftToastImaged: Asset.toastSatusMicrophoneOffIcon.image, rightToastImage: Asset.toastSatusCameraOffIcon.image, infoLabel: ServiceLocator.localizable.microphoneDesactivated) // swiftlint:disable:this line_length
                    }
                default:
                    break
                }

            }
        }
    }

    @IBAction private func enableAndDisableCameraAction(_ sender: Any) {
        if let publisher = self.publisher {
            self.notifyToggleVideo()
            publisher.publishVideo = !self.publishVideoAudio
            if !publisher.publishVideo {
                self.publishVideoAudio = false
                self.enableButton(on: cameraButton, with: Asset.cameraOffIconStack.image, with: ColorName.redRejected.color)
                if !publisher.publishVideo && !publisher.publishAudio {
                    self.configToastCentralPublisher(cameraButton, leftToastImaged: Asset.toastSatusMicrophoneOffIcon.image, rightToastImage: Asset.toastSatusCameraOffIcon.image, infoLabel: ServiceLocator.localizable.cameraAndMicrophoneDeactivated) // swiftlint:disable:this line_length
                } else {
                    self.configToastCentralPublisher(cameraButton, leftToastImaged: Asset.toastSatusMicrophoneOffIcon.image, rightToastImage: Asset.toastSatusCameraOffIcon.image, infoLabel: ServiceLocator.localizable.cameraDeactivated) // swiftlint:disable:this line_length
                }
            } else {
                self.publishVideoAudio = true
                self.disableButton(on: cameraButton, with: Asset.cameraOnIconStack.image)
            }
        }
    }

    internal func configToastCentralPublisher( _ toastView: UIView, leftToastImaged: UIImage?, rightToastImage: UIImage?, infoLabel: String) {
        if let publisher = self.publisher {
            self.rightToastCentralImage.isHidden = publisher.publishVideo
            self.leftToastCentralImage.isHidden = publisher.publishAudio
            self.toastCentralInfoView.isHidden = false
            self.infoStatusToastDevicesOnCallLabel.isHidden = false
            self.rightToastCentralImage.image = rightToastImage
            self.leftToastCentralImage.image = leftToastImaged
            self.infoStatusToastDevicesOnCallLabel.text = infoLabel
            self.toastCentralInfoView.fadeIn(alpha: 1)
            self.toastCentralInfoView.fadeOut(alpha: 1)
        }

    }

    internal func configToastCentralSubscriber( _ toastView: UIView, leftToastImage: UIImage?, rightToastImage: UIImage?, infoLabel: String) {
        if let subscriber = self.subscriber {
            self.rightToastCentralImage.isHidden = !subscriber.subscribeToVideo
            self.leftToastCentralImage.isHidden = !subscriber.subscribeToAudio
            self.toastCentralInfoView.isHidden = false
            self.infoStatusToastDevicesOnCallLabel.isHidden = false
            self.rightToastCentralImage.image = rightToastImage
            self.leftToastCentralImage.image = leftToastImage
            self.infoStatusToastDevicesOnCallLabel.text = infoLabel
            self.toastCentralInfoView.fadeIn(alpha: 1)
        }

    }

    @IBAction private func swapCameraOrSpeakerAction(_ sender: Any) {
        switch self.callType {
        case .call:
            if self.isAciveMic {
                self.isAciveMic = !self.isAciveMic
                self.setAudioOutput(enabledMic: isAciveMic)
                self.enableButton(on: swapCameraOrSpeakerButton, with: Asset.speakerOnIconStack.image, with: ColorName.background.color)
                self.configToastCentralPublisher(cameraButton, leftToastImaged: nil, rightToastImage: Asset.toastStatusSpeakerOffIcon.image, infoLabel: ServiceLocator.localizable.speakerActivated) // swiftlint:disable:this line_length
            } else {
                self.isAciveMic = !self.isAciveMic
                self.setAudioOutput(enabledMic: isAciveMic)
                self.disableButton(on: swapCameraOrSpeakerButton, with: Asset.speakerOffIconStack.image)
            }
        case .video:
            self.notifySwapCamera()
             if let publisher = self.publisher {
                if publisher.cameraPosition == .front {
                    self.publisher?.cameraPosition = .back
                    self.enableButton(on: swapCameraOrSpeakerButton, with: Asset.reverseCameraOnconStack.image, with: ColorName.purple.color)
                } else {
                    self.disableButton(on: swapCameraOrSpeakerButton, with: Asset.reverseCameraOffIconStack.image)
                    self.publisher?.cameraPosition = .front
                }
            }
        default:
            CoreLog.business.error("Error to load button swapCameraOrSpeakerAction")
        }
    }

    @IBAction private func endOrRejectCallAction (_ sender: Any) {
        switch self.userType {
        case .patient:
            self.endCall()
        case .professional:
            switch self.configuration {
            case .inProgressCall, .inProgressVideo:
                self.endCall()
            default:
                self.rejectCall()
            }
        default: break
        }
    }

    @IBAction private func rejectCallAction(_ sender: Any) {
        self.rejectCall()
    }

    @IBAction private func acceptCallAction(_ sender: Any) {
        VideoCall.isInProgress = true
        self.changeStatus { [weak self] in
            guard let self = self else { return }
            if let userType = self.userType {
                self.presenter?.pickUp(for: userType, by: self.callId) { [weak self] result in
                    guard let self = self else { return }
                    if case .success = result {
                        self.connect()
                    }
                    if case let .failure(error) = result {
                        CoreLog.business.error("%@", error.localizedDescription)
                    }
                }
            }
        }
    }
}

extension MainViewController {
    private func enableAutoLockScreen() {
        UIApplication.shared.isIdleTimerDisabled = false
    }

    private func disableAutoLockScreen() {
        UIApplication.shared.isIdleTimerDisabled = true
    }

    internal func changeStatus(completion: () -> Void) {
        defer {
            completion()
        }

        switch self.configuration {
        case .incomingCall where self.userType == .patient,
             .outgoingCall where self.userType == .professional:
            self.configuration = .inProgressCall(VideoCallInProgressCallConfiguration())
        case .incomingVideo where self.userType == .patient,
             .outgoingVideo where self.userType == .professional:
            self.configuration = .inProgressVideo(VideoCallInProgressVideoConfiguration())
        case .incomingCall where self.userType == .professional,
             .outgoingCall where self.userType == .professional:
            self.configuration = .inProgressCall(VideoCallInProgressCallConfiguration())
        case .incomingVideo where self.userType == .professional,
             .outgoingVideo where self.userType == .professional:
            self.configuration = .inProgressVideo(VideoCallInProgressVideoConfiguration())
        default:
            break
        }
    }
}

extension MainViewController {
    private func endCall() {
        VideoCall.isInProgress = false
        if let userType = self.userType, let callId = self.callId {
            self.presenter?.hangUp(for: userType, by: callId) { _ in
                self.notifyHangUp()
            }
        }
        self.disconnect()
    }

    internal func rejectCall() {
        VideoCall.isInProgress = false

        func reject() {
            if let userType = self.userType, let callId = self.callId {
                DispatchQueue.main.async {
                    if let currentViewController = UIApplication.topViewController(),
                       currentViewController is mediquo_videocall_lib.MainViewController || userType == .patient {
                        self.dismiss(animated: true)
                    }
                }
                let permission = PermissionRequest(permissions: Permission(audio: self.audioPermission, video: self.videoPermission))
                self.presenter?.reject(for: userType, by: callId, permission: permission) { _ in
                    self.notifyReject()
                    self.notifySdkEnded()
                }
            }
        }

        self.statusCallLabel.text = self.callType == .call ? ServiceLocator.localizable.rejectedCall : ServiceLocator.localizable.rejectedVideoCall
        self.userPermissions?.checkMicrophoneAvailable { audioPermission in
            self.audioPermission = audioPermission
            switch self.callType {
            case .call:
                reject()
            case .video:
                self.userPermissions?.checkVideoCameraAvailable { videoPermission in
                    self.videoPermission = videoPermission
                    reject()
                }
            default: break
            }
        }
    }
}

extension MainViewController {
    internal func goToCreateReport() {
        guard self.userType == .professional else { return }

        weak var pvc = self.presentingViewController
        self.dismiss(animated: true) {
            if self.enableReport == true {
                guard let uuid = self.callId,
                      let time = self.timeTotalOnCallLabel.text,
                      let name = self.customer?.name,
                      let controller = ServiceLocator.getCreateReport(model: CreateReportModel(uuid: uuid, time: time, name: name)) else { return }
                pvc?.present(controller, animated: true)
            }
        }
    }
}

extension MainViewController {
    internal func dismissSdkOnEndCall() {
        guard self.userType == .patient, ServiceLocator.isSdk else { return }
        self.dismiss(animated: true)
    }
}

extension MainViewController {
    internal func goToRating() {
        guard self.userType == .patient, !ServiceLocator.isSdk else { return }

        weak var pvc = self.presentingViewController
        self.dismiss(animated: true) {
            guard let callType = self.callType,
                  let callId = self.callId,
                  let avatar = self.professional?.avatar,
                  let name = self.professional?.name,
                  let time = self.timeTotalOnCallLabel.text else { return }

            switch callType {
                case .call:
                    guard let controller = ServiceLocator.getRatingCall(callId: callId, avatar: avatar, name: name, time: time) else { return }
                    controller.mediquoSessionDelegate = self.mediquoSessionDelegate
                    pvc?.present(controller, animated: true)
                case .video:
                    guard let controller = ServiceLocator.getRatingVideo(callId: callId, avatar: avatar, name: name, time: time) else { return }
                    controller.mediquoSessionDelegate = self.mediquoSessionDelegate
                    pvc?.present(controller, animated: true)
            }
        }
    }
}

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return self.topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return self.topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return self.topViewController(presented)
        }
        return base
    }
} // swiftlint:disable:this file_length
