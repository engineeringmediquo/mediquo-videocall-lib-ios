//
//  RatingViewController.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 22/12/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit
import Foundation

public class RatingViewController: VideoCallBaseViewController {

    @IBOutlet private var contactView: UIImageView! {
        didSet {
            self.contactView.backgroundColor = ColorName.lightPurple.color
            self.contactView.circularShape()
        }
    }

    @IBOutlet private var nameLabel: UILabel! {
        didSet {
            self.nameLabel.textColor = .white
            self.nameLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 16)
        }
    }

    @IBOutlet private var callLabel: UILabel! {
        didSet {
            self.callLabel.text = ServiceLocator.localizable.ratingTitle.uppercased()
            self.callLabel.textColor = .white
            self.callLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 12)
        }
    }

    @IBOutlet private var timerLabel: UILabel! {
        didSet {
            self.timerLabel.textColor = .white
            self.timerLabel.cornerRounded()
            self.timerLabel.backgroundColor = .black
            self.timerLabel.font = UIFont(name: FontFamily.GothamRounded.book.name, size: 14)
            self.timerLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }

    @IBOutlet private var messageLabel: UILabel! {
        didSet {
            self.messageLabel.text = ServiceLocator.localizable.ratingMessage
            self.messageLabel.textColor = .white
            self.messageLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 18)
        }
    }

    @IBOutlet private var goodButton: UIButton! {
        didSet {
            self.goodButton.backgroundColor = ColorName.indigoBlue.color
            self.goodButton.layer.cornerRadius = 10
            self.goodButton.setTitleColor(.white, for: .normal)
            self.goodButton.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 18)
            self.goodButton.setTitle(ServiceLocator.localizable.ratingGood, for: .normal)
            self.goodButton.contentHorizontalAlignment = .center
            self.goodButton.contentVerticalAlignment = .center
        }
    }

    @IBOutlet private var improvableButton: UIButton! {
        didSet {
            self.improvableButton.backgroundColor = ColorName.indigoBlue.color
            self.improvableButton.layer.cornerRadius = 10
            self.improvableButton.setTitleColor(.white, for: .normal)
            self.improvableButton.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 18)
            self.improvableButton.setTitle(ServiceLocator.localizable.ratingImprovable, for: .normal)
            self.improvableButton.contentHorizontalAlignment = .center
            self.improvableButton.contentVerticalAlignment = .center
        }
    }

    @IBOutlet private var exitButton: UIButton! {
        didSet {
            self.exitButton.titleLabel?.text = nil
        }
    }

    @IBAction func exit(_ sender: Any) {
        self.notifyClose()
        self.mediquoSessionDelegate?.streamRated()
        self.dismiss(animated: true)
    }

    @IBAction func didTapGood(_ sender: Any) {
        self.notifyGood()
        self.mediquoSessionDelegate?.streamRated()
        self.dismiss(animated: true)
    }

    @IBAction func didTapImprovable(_ sender: Any) {
        self.notifyImprovable()
        self.mediquoSessionDelegate?.streamRated()
        self.dismiss(animated: true)
    }

    var callType: CallType?
    var callId: String?

    var avatar: String?
    var name: String?
    var time: String?
    var mediquoSessionDelegate: MediquoSessionDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.notifyRatingView()
        self.initBackground()
        self.fillData()
    }

    private func initBackground() {
        self.view.backgroundColor = ColorName.ratingBackground.color
    }

    private func fillData() {
        if let avatar = self.avatar, let url = URL(string: avatar) {
           self.contactView.load(url: url)
        }
        self.nameLabel.text = self.name
        self.timerLabel.text = time
    }
}

// MARK: - Tracking

extension RatingViewController {
    internal func notifyRatingView() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Rating.View, object: nil, userInfo: dataDict)
    }

    internal func notifyClose() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Rating.Close, object: nil, userInfo: dataDict)
    }

    internal func notifyGood() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Rating.Good, object: nil, userInfo: dataDict)
    }

    internal func notifyImprovable() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Rating.Improvable, object: nil, userInfo: dataDict)
    }
}
