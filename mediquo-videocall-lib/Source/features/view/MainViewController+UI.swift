//
//  MainViewController+UI.swift
//  mediquo-videocall-lib
//
//  Created by JORDI GALLEN RENAU on 29/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit
import AVFoundation

extension MainViewController {
    internal func configBackgroundView(with view: UIView, and image: UIImage, with alpha: CGFloat = 0) {
        view.cornerRounded()
        view.backgroundColor = UIColor.white.withAlphaComponent(alpha)
        let imageView:UIImageView = UIImageView()
        imageView.image = image
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.frame.size.width = 24
        imageView.frame.size.height = 24
        view.addSubview(imageView)
    }

    internal func configBackgroundButton(with button: UIButton, and image: UIImage, and colorBackground: UIColor, with alpha: CGFloat) {
        button.cornerRounded()
        button.backgroundColor = colorBackground.withAlphaComponent(alpha)
        button.setImage(image, for: .normal)
    }

    internal func configBackgroundView(with view: UIView, alpha: CGFloat) {
        view.cornerRounded()
        view.backgroundColor = UIColor.black.withAlphaComponent(alpha)
    }

    internal func backGroundWhiteButton(_ button: UIButton) {
        button.backgroundColor = .white
    }

    internal func backgroundHeader() {
        let colorTop: CGColor = UIColor.black.withAlphaComponent(1).cgColor
        let colorBottom: CGColor = UIColor.black.withAlphaComponent(0).cgColor
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.backgroundHeaderView.bounds
        self.backgroundHeaderView.layer.insertSublayer(gradientLayer, above: nil)
    }

    internal func refreshScene() {
        var userName: String?
        var statusCall: String?

        switch self.userType {
        case .patient:
            userName = self.professional?.name
            statusCall = self.callType == .call ? ServiceLocator.localizable.incomingCall : ServiceLocator.localizable.incomingVideoCall
        case .professional:
            if let customer = self.customer {
                userName = customer.name
            } else if let professional = self.professional {
                userName = professional.name
            }
            statusCall = self.callType == .call ? ServiceLocator.localizable.calling : ServiceLocator.localizable.videoCalling
        default: break
        }

        if let userName = userName {
            self.nameDoctorLabel.text = userName
            self.namePatientOrProfessionalLabel.text = userName
        } else {
            self.nameDoctorLabel.isHidden = true
            self.namePatientOrProfessionalLabel.isHidden = true
        }

        if let statusCall = statusCall {
            self.statusCallLabel.text = statusCall
        } else {
            self.statusCallLabel.isHidden = true
        }

        if let image = customer?.avatar,
           let url = URL(string: image) {
            self.professionalImageView.load(url: url)

        } else if let image = professional?.avatar,
           let url = URL(string: image) {
            self.professionalImageView.load(url: url)
        }

        self.firstLetterPatientView.text = self.getFirstLetter(from: userName ?? "")

        self.setVisibility()
    }

    internal func setAudioOutput(enabledMic: Bool) {
        let session = AVAudioSession.sharedInstance()
        try? session.setCategory(AVAudioSession.Category.playAndRecord)
        try? session.setMode(AVAudioSession.Mode.voiceChat)
        if enabledMic {
            self.setAudioOutputMicrophoneOrSpeaker(enabledMic: enabledMic)
        } else {
            try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        }
        try? session.setActive(true)
    }

    func setAudioOutputMicrophoneOrSpeaker(enabledMic: Bool) {
        let session = AVAudioSession.sharedInstance()
        guard let availableInputs = session.availableInputs else { return }
        var mic : AVAudioSessionPortDescription?
        for input in availableInputs where input.portType == AVAudioSession.Port.headsetMic {
            mic = input
        }
        guard mic != nil else { return }
        try? session.setPreferredInput(mic)
        try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
    }

   private func setVisibility() {
        guard let config = self.configScreen else { return }
        self.fullScreenCameraView.isHidden = config.isHiddenFullScreenCameraView
        self.littleScreenCameraView.isHidden = config.isHiddenLittleScreenCameraView
        self.nameDoctorLabel.isHidden = config.isHiddenNameDoctorLabel
        self.firstLetterPatientView.isHidden = config.isHiddenFirstLetterPatientView
        self.professionalImageView.isHidden = config.isHiddenProfessionalImageView
        self.namePatientOrProfessionalLabel.isHidden = config.isHiddenNamePatientOrProfessionalLabel
        self.statusCallLabel.isHidden = config.isHiddenStatuCallLabel
        self.microStackButton.isHidden = config.isHiddenMicroStackButton
        self.cameraButton.isHidden = config.isHiddenCameraButton
        self.swapCameraOrSpeakerButton.isHidden = config.isHiddenSwapCameraButton
        self.endOrRejectCallButton.isHidden = config.isHiddenEndOrRejectCallButton
        self.rejectCallButton.isHidden = config.isHiddenRejectCallButton
        self.acceptCallButton.isHidden = config.isHiddenAcceptCallButton
        self.rejectCallOrVideoCallLabel.isHidden = config.isHiddenRejectCallOrVideoCallLabel
        self.acceptCallOrVideoCallLabel.isHidden = config.isHiddenAcceptCallOrVideoCallLabel
    }
}

extension MainViewController {
    internal func configPatientLabel(with label: UILabel, stringLetter: String, sizeLetter: CGFloat, and fontLetter: String, colorLetterText: UIColor) {
        label.text = stringLetter
        label.font = UIFont(name: fontLetter, size: sizeLetter)
        label.textColor = colorLetterText
    }

    private func getFirstLetter(from name: String) -> String {
        return name.first?.uppercased() ?? ""
    }

    internal func enableButton(on button:UIButton, with imageButton: UIImage, with tintColor: UIColor) {
        self.backGroundWhiteButton(button)
        button.setImage(imageButton, for: .normal)
        button.tintColor = tintColor
    }

    internal func disableButton(on button:UIButton, with imageButton: UIImage) {
        self.configBackgroundButton(with: button, and: imageButton, and: ColorName.black.color, with: 0.5)
        button.tintColor = .white
    }
}

extension MainViewController {
    internal func prepareTimer() {
        self.timeTotalOnCallLabel.isHidden = false
        self.timeTotalOnCallLabel.text = String(self.counter)
        self.timeTotalOnCallLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 14)
        self.ringSoundTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCounter), userInfo: nil, repeats: true)
    }

    @objc private func updateCounter() {
        self.counter += 1
        dateFormatter.dateFormat = "mm:ss"
        let timeInterval = Double(self.counter)
        let myNSDate = Date(timeIntervalSince1970: timeInterval)
        self.timeTotalOnCallLabel.text = dateFormatter.string(from: myNSDate)
    }
}

extension MainViewController {
    internal func pollingSubscriber() {
        self.pollingCheckStatusTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(hasAudioAndVideoSubscriber), userInfo: nil, repeats: true)
    }

    @objc private func hasAudioAndVideoSubscriber() {
        switch self.callType {
        case .call:
            self.setUpCentralToastCall()
        case .video:
            self.setUpCentralToastVideo()
        default:
            break
        }
    }

    private func setUpCentralToastCall() {
        if let subscriber = self.subscriber, let streamSubscriber = subscriber.stream {
            if !streamSubscriber.hasAudio {
                let labelInfo = self.userType == .patient ? ServiceLocator.localizable.microCustomerLabel : ServiceLocator.localizable.microProLabel
                self.configToastCentralSubscriber(microStackButton, leftToastImage: nil, rightToastImage: Asset.toastSatusMicrophoneOffIcon.image, infoLabel:labelInfo) // swiftlint:disable:this line_length
            } else {
                self.hiddenCentralToast()
            }
        }
    }

    private func setUpCentralToastVideo() {
        if let subscriber = self.subscriber, let streamSubscriber = subscriber.stream {
            if !streamSubscriber.hasAudio, !streamSubscriber.hasVideo {
                let labelInfo = self.userType == .patient ? ServiceLocator.localizable.microAndCamaraCustomerLabel : ServiceLocator.localizable.microAndCamaraProLabel
                self.configToastCentralSubscriber(microStackButton, leftToastImage: Asset.toastSatusMicrophoneOffIcon.image, rightToastImage: Asset.toastSatusCameraOffIcon.image, infoLabel:labelInfo) // swiftlint:disable:this line_length
            } else if !streamSubscriber.hasVideo {
                let labelInfo = self.userType == .patient ? ServiceLocator.localizable.camaraCustomerLabel : ServiceLocator.localizable.camaraProLabel
                self.configToastCentralSubscriber(microStackButton, leftToastImage: nil, rightToastImage: Asset.toastSatusCameraOffIcon.image, infoLabel: labelInfo) // swiftlint:disable:this line_length
            } else if !streamSubscriber.hasAudio {
                let labelInfo = self.userType == .patient ? ServiceLocator.localizable.microCustomerLabel : ServiceLocator.localizable.microProLabel
                self.configToastCentralSubscriber(microStackButton, leftToastImage: nil, rightToastImage: Asset.toastSatusMicrophoneOffIcon.image, infoLabel: labelInfo) // swiftlint:disable:this line_length
            } else {
                self.hiddenCentralToast()
            }
        }
    }

    private func hiddenCentralToast() {
        self.toastCentralInfoView.isHidden = true
        self.leftToastCentralImage.isHidden = true
        self.rightToastCentralImage.isHidden = true
        self.infoStatusToastDevicesOnCallLabel.isHidden = true
    }
}
