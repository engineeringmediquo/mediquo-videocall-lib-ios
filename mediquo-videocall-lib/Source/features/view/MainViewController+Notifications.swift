//
//  MainViewController+Notifications.swift
//  mediquo-videocall-lib
//
//  Created by Chus Clua on 11/08/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

extension MainViewController {
    internal func notifyCallView() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.View, object: nil, userInfo: dataDict)
    }

    internal func notifyPickUp() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.PickUp, object: nil, userInfo: dataDict)
    }

    internal func notifyHangUp() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.End, object: nil, userInfo: dataDict)
    }

    internal func notifyReject() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.Reject, object: nil, userInfo: dataDict)
    }

    internal func notifyToggleAudio() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.ToggleAudio, object: nil, userInfo: dataDict)
    }

    internal func notifyToggleVideo() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.ToggleVideo, object: nil, userInfo: dataDict)
    }

    internal func notifySwapCamera() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.SwapCamera, object: nil, userInfo: dataDict)
    }

    internal func notifyPermissionAudioRequest() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.PermissionAudioRequest, object: nil, userInfo: dataDict)
    }

    internal func notifyPermissionAudioAccept() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.PermissionAudioAccept, object: nil, userInfo: dataDict)
    }

    internal func notifyPermissionVideoRequest() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.PermissionVideoRequest, object: nil, userInfo: dataDict)
    }

    internal func notifyPermissionVideoAccept() {
        let dataDict: [String: Any] = [Notification.Name.MediQuoVideoCallKey.CallType: self.callType?.rawValue ?? "",
                                       Notification.Name.MediQuoVideoCallKey.CallId: self.callId ?? ""]
        NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Call.PermissionVideoAccept, object: nil, userInfo: dataDict)
    }
}

// MARK: - Sdk events

extension MainViewController {
    internal func notifySdkStarted() {
        switch self.callType {
        case .call:
            NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Sdk.Call.Started, object: nil)
        case .video:
            NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Sdk.VideoCall.Started, object: nil)
        case .none:
            break
        }
    }

    internal func notifySdkEnded() {
        switch self.callType {
        case .call:
            NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Sdk.Call.Ended, object: nil)
        case .video:
            NotificationCenter.default.post(name: Notification.Name.MediQuoVideoCall.Sdk.VideoCall.Ended, object: nil)
        case .none:
            break
        }
    }
}
