//
//  MainViewController+Opentok.swift
//  mediquo-videocall-lib
//
//  Created by JORDI GALLEN RENAU on 29/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit
import OpenTok

extension MainViewController: OTSessionDelegate {
    public func sessionDidConnect(_ session: OTSession) {
        CoreLog.business.info("Session connected")
        guard let callType = self.callType else { return }
        switch callType {
        case .call:
            self.initPromiseForAudioPermission()
        case .video:
            self.initPromiseForVideoPermission()
        }
    }

    public func sessionDidDisconnect(_ session: OTSession) {
        CoreLog.business.info("Session disconnected")
    }

    public func session(_ session: OTSession, didFailWithError error: OTError) {
        CoreLog.business.error("Session Failed to connect: %@", error.localizedDescription)
        VideoCall.isInProgress = false
    }

    public func session(_ session: OTSession, streamCreated stream: OTStream) {
        CoreLog.business.info("Session streamCreated: %@", stream.streamId)
        if self.subscriber == nil {
            VideoCall.isInProgress = true
            self.mediaController?.stopSound()
            self.doSubscribe(stream)
        }
    }

    public func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        CoreLog.business.info("Session streamDestroyed: %@", stream.streamId)
        VideoCall.isInProgress = false
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            self.cleanupSubscriber()
        }
        self.ringSoundTimer.invalidate()
        self.goToRating()
        self.dismissSdkOnEndCall()
        self.goToCreateReport()
        self.notifySdkEnded()
        self.mediquoSessionDelegate?.streamDestroyed()
    }

    private func cleanupSubscriber() {
        self.subscriber?.view?.removeFromSuperview()
        self.subscriber = nil
    }

    private func cleanupPublisher() {
        self.publisher?.view?.removeFromSuperview()
    }
}

extension MainViewController: OTPublisherDelegate {
    public func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        CoreLog.business.info("Publisher streamCreated: %@", stream.streamId)
        VideoCall.isInProgress = true
        switch self.callType {
        case .call:
            self.fullScreenCameraView.backgroundColor = ColorName.background.color
        case .video:
            guard let fullView: UIView = self.subscriber?.view else { return }
            fullView.frame = CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: Int(UIScreen.main.bounds.height))
            self.fullScreenCameraView.insertSubview(fullView, at: 0)
        default:
            self.fullScreenCameraView.backgroundColor = ColorName.background.color
        }
        self.configBackgroundView(with: self.timeTotalOnCallLabel, alpha: 0.5)
    }

    public func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        CoreLog.business.info("Publisher streamDestroyed: %@", stream.streamId)
        VideoCall.isInProgress = false
        self.cleanupPublisher()
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            self.cleanupSubscriber()
        }
        self.ringSoundTimer.invalidate()
        self.goToRating()
        self.dismissSdkOnEndCall()
        self.goToCreateReport()
        self.notifySdkEnded()
    }

    public func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        CoreLog.business.error("The publisher failed: %@", error.localizedDescription)
        VideoCall.isInProgress = false
    }
}

extension MainViewController: OTSubscriberDelegate {
    public func subscriberDidConnect(toStream stream: OTSubscriberKit) {
        self.changeStatus { [weak self] in
            guard let self = self else { return }
            self.notifyPickUp()
            self.notifySdkStarted()
            switch self.callType {
            case .call:
                self.setAudioOutput(enabledMic: true)
                self.fullScreenCameraView.backgroundColor = ColorName.background.color
            case .video:
                self.setAudioOutput(enabledMic: false)
                guard let publisherView = self.publisher?.view else { return }
                publisherView.frame = CGRect(x: 0, y: 0, width: self.littleScreenCameraView.frame.width, height: self.littleScreenCameraView.frame.height)
                self.littleScreenCameraView.insertSubview(publisherView, at: 0)
                self.littleScreenCameraView.isHidden = false

                guard let subscriberView: UIView = self.subscriber?.view else { return }
                subscriberView.frame = CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: Int(UIScreen.main.bounds.height))
                self.fullScreenCameraView.insertSubview(subscriberView, at: 0)

                self.nameDoctorLabel.isHidden = false
                self.professionalImageView.isHidden = true
                self.namePatientOrProfessionalLabel.isHidden = true
            default:
                self.fullScreenCameraView.backgroundColor = ColorName.background.color
            }
        }
        self.prepareTimer()
        self.configBackgroundView(with: self.timeTotalOnCallLabel, alpha: 0.5)
    }

    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        CoreLog.business.error("Subscriber failed: %@", error.localizedDescription)
    }
}

extension MainViewController {
    internal func doPublish() {
        var error: OTError?
        defer {
            processError(error)
        }

        guard let publisher = self.publisher else { return }
        self.session?.publish(publisher, error: &error)
        switch self.callType {
        case .call:
            self.fullScreenCameraView.backgroundColor = ColorName.background.color
        case .video:
            if let pubView = publisher.view {
                switch self.userType {
                case .patient:
                    pubView.frame = CGRect(x: 0, y: 0, width: self.littleScreenCameraView.frame.width, height: self.littleScreenCameraView.frame.height)
                    self.publisherView = pubView
                    if let publisher = self.publisherView {
                        self.littleScreenCameraView.insertSubview(publisher, at: 0)
                    }
                case .professional:
                    pubView.frame = CGRect(x: 0, y: 0, width: self.fullScreenCameraView.frame.width, height: self.fullScreenCameraView.frame.height)
                    self.publisherView = pubView
                    if let publisher = self.publisherView {
                        self.fullScreenCameraView.insertSubview(publisher, at: 0)
                    }
                default: break
                }
            }
        default:
            self.fullScreenCameraView.backgroundColor = ColorName.background.color
        }
    }

    private func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            self.processError(error)
        }
        self.subscriber = OTSubscriber(stream: stream, delegate: self)
        if let subscriber = self.subscriber {
            self.session?.subscribe(subscriber, error: &error)
        }
    }
}

extension MainViewController {
    internal func connect() {
        guard self.session?.sessionConnectionStatus != .connected else { return }

        var error: OTError?
        guard let token = self.token else { return }
        self.session?.connect(withToken: token, error: &error)

        guard error == nil else {
            CoreLog.business.error("%@", error?.debugDescription ?? "An error occurred")
            return
        }
    }

    internal func disconnect() {
        var error: OTError?
        self.session?.disconnect(&error)

        guard error == nil else {
            CoreLog.business.error("%@", error?.debugDescription ?? "An error occurred")
            return
        }
    }
}

extension MainViewController {
    private func processError(_ error: OTError?) {
        guard let error = error else { return }
        DispatchQueue.main.async {
            let controller = UIAlertController(title: ServiceLocator.localizable.error, message: error.localizedDescription, preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: ServiceLocator.localizable.ok, style: .default, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
    }
}
