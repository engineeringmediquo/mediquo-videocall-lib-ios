//
//  BusyPopupViewController.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 16/12/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

class BusyPopupViewController: VideoCallBaseViewController {
    @IBOutlet internal var backgrounView: UIView! {
        didSet {
            self.backgrounView.cornerRounded()
        }
    }

    @IBOutlet internal var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = ServiceLocator.localizable.popupBusyTitle
            self.titleLabel.textColor = ColorName.indigoBlue.color
            self.titleLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 18)
        }
    }

    @IBOutlet internal var subtitlelabel: UILabel! {
        didSet {
            self.subtitlelabel.addLineSpacingTo(text: ServiceLocator.localizable.popupBusySubtitle,
                                                font: FontFamily.GothamRounded.book.font(size: 14),
                                                color: ColorName.lightGrey.color)
        }
    }

    @IBOutlet internal var performActionButton: UIButton! {
        didSet {
            self.performActionButton.setTitle(ServiceLocator.localizable.popupBusyButton, for: .normal)
            self.performActionButton.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 18)
            self.performActionButton.titleLabel?.textColor = .white
            self.performActionButton.backgroundColor = ColorName.indigoBlue.color
            self.performActionButton.layer.cornerRadius = 8
            self.performActionButton.clipsToBounds = true
        }
    }

    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
