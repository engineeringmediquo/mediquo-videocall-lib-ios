//
//  VideoCallManager.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

public enum VideoCallManagerState {
    case onAvailable
    case onBackground
    case callRejected
    case callRequested
    case callPickedUp
}

public protocol VideoCallManagerProtocol: AnyObject {

    // MARK: - Actions

    func showOutgoingCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (VideoCallBaseViewController?) -> Void)
    func rejectOutgoingCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (Bool) -> Void)

    // MARK: - Patient Actions

    func showIncomingCall(by userType: UserType, by schema: VideoCallNotificationSchema) -> MainViewController?
    func rejectIncomingCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (Bool) -> Void)
    func showInProgressCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (MainViewController?) -> Void)

    // MARK: - Push Notifications Manager

    func processPushNotification(role: UserType, currentViewController: UIViewController, userInfo: [AnyHashable: Any],
                                 completion: @escaping (UNNotificationPresentationOptions) -> Void)
    func processTapPushNotification(role: UserType, currentViewController: UIViewController, userInfo: [AnyHashable: Any],
                                    completionHandler: @escaping () -> Void)

}

internal class VideoCallManager: VideoCallManagerProtocol {

    private let presenter: ServicesPresenterProtocol?

    init(presenter: ServicesPresenterProtocol) {
        self.presenter = presenter
    }
}

// MARK: - Actions

extension VideoCallManager {
    public func showOutgoingCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (VideoCallBaseViewController?) -> Void) {
        guard let presenter = self.presenter, let callType = schema.call?.type, let roomId = schema.call?.roomId else {
            completion(nil)
            return
        }

        switch callType {
        case .call:
            presenter.call(by: roomId) { result in
                if case let .success(response) = result, let call = response.call, let session = response.session {
                    let sessionId = session.sessionId
                    let callId = call.uuid
                    let token = session.customerToken
                    let customer = schema.customer
                    let viewController = ServiceLocator.getOutgoingCall(userType: userType, sessionId: sessionId, callId: callId, token: token, customer: customer)
                    viewController?.enableReport = schema.enableReport
                    completion(viewController)
                }
                if case let .failure(error) = result {
                    CoreLog.business.error("%@", error.localizedDescription)
                    let viewController = ServiceLocator.getBusyPopup()
                    completion(viewController)
                }
            }
        case .video:
            presenter.videoCall(by: roomId) { result in
                if case let .success(response) = result, let call = response.call, let session = response.session {
                    let sessionId = session.sessionId
                    let callId = call.uuid
                    let token = session.customerToken
                    let customer = schema.customer
                    let viewController = ServiceLocator.getOutgoingVideo(userType: userType, sessionId: sessionId, callId: callId, token: token, customer: customer)
                    viewController?.enableReport = schema.enableReport
                    completion(viewController)
                }
                if case let .failure(error) = result {
                    CoreLog.business.error("%@", error.localizedDescription)
                    let viewController = ServiceLocator.getBusyPopup()
                    completion(viewController)
                }
            }
        }
    }

    public func rejectOutgoingCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (Bool) -> Void) {
        self.rejectCall(by: userType, by: schema, completion: completion)
    }
}

// MARK: - Patient Actions

extension VideoCallManager {
    public func showIncomingCall(by userType: UserType, by schema: VideoCallNotificationSchema) -> MainViewController? {
        guard let callType = schema.call?.type else { return nil }

        let sessionId = schema.call?.sessionId
        let callId = schema.call?.uuid
        let token = schema.call?.token
        let professional = schema.professional
        switch callType {
        case .call:
            return ServiceLocator.getIncomingCall(userType: userType, sessionId: sessionId, callId: callId, token: token, professional: professional)
        case .video:
            return ServiceLocator.getIncomingVideo(userType: userType, sessionId: sessionId, callId: callId, token: token, professional: professional)
        }
    }

    public func rejectIncomingCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (Bool) -> Void) {
        self.rejectCall(by: userType, by: schema, completion: completion)
    }

    public func showInProgressCall(by userType: UserType, by schema: VideoCallNotificationSchema, completion: @escaping (MainViewController?) -> Void) {
        guard let presenter = self.presenter else { completion(nil); return }
        presenter.pickUp(for: userType, by: schema.call?.uuid) { result in
            if case .success = result {
                let viewController = self.getInProgressCallViewController(userType: userType,
                                                                          schema: schema)
                completion(viewController)
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.localizedDescription)
            }
        }
    }
}

// MARK: - Professional Reports

extension VideoCallManager {
    func getCreateReport(model: CreateReportModel?) -> CreateReportViewController? {
        return ServiceLocator.getCreateReport(model: model)
    }
}

// MARK: - Push Notifications Manager

extension VideoCallManager {
    func processPushNotification(role: UserType, currentViewController: UIViewController, userInfo: [AnyHashable: Any],
                                 completion: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard let schema = self.processSchema(userInfo: userInfo) else {
            completion([])
            return
        }

        switch self.getVideoCallManagerState(schema: schema) {
        case .onAvailable:
            self.showIncomingCall(currentViewController: currentViewController, by: role, by: schema)
            completion([])
            return

        case .onBackground:
            completion([.alert, .sound])
            return

        case .callRejected:
            if currentViewController is MainViewController {
                currentViewController.dismiss(animated: true)
            }
            completion([])
            return

        case .callRequested:
            if VideoCall.isInProgress, role == .professional {
                weak var pvc = currentViewController.presentingViewController
                pvc?.dismiss(animated: true, completion: nil)
            }
            completion([])
            return

        case .callPickedUp:
            if !VideoCall.isInProgress {
                weak var pvc = currentViewController.presentingViewController
                pvc?.dismiss(animated: true, completion: nil)
            }
            completion([])
            return
        }
    }

    func processTapPushNotification(role: UserType, currentViewController: UIViewController, userInfo: [AnyHashable: Any],
                                    completionHandler: @escaping () -> Void) {
        guard let schema = self.processSchema(userInfo: userInfo) else {
            completionHandler()
            return
        }

        switch self.getVideoCallManagerState(schema: schema) {
        case .onAvailable, .callRejected, .callRequested, .callPickedUp:
            break

        case .onBackground:
            self.showIncomingCall(currentViewController: currentViewController, by: role, by: schema)
            completionHandler()
            return
        }
    }
}

// MARK: - Private functions (Call Actions)

extension VideoCallManager {

    private func rejectCall(by userType: UserType,
                            by schema: VideoCallNotificationSchema,
                            completion: @escaping (Bool) -> Void) {

        guard let presenter = self.presenter else { completion(false); return }
        let callId = schema.call?.uuid
        let permission: PermissionRequest = PermissionRequest(permissions: schema.permissions ?? Permission(audio: false, video: false))
        presenter.reject(for: userType, by: callId, permission: permission) { result in
            switch result {
            case .success:
                completion(true)
            case .failure(let error):
                CoreLog.business.error("%@", error.localizedDescription)
                completion(false)
            }
        }
    }

    private func showIncomingCall(currentViewController: UIViewController,
                                  by userType: UserType,
                                  by schema: VideoCallNotificationSchema) {

        guard let callType = schema.call?.type else { return }

        let sessionId = schema.call?.sessionId
        let callId = schema.call?.uuid
        let token = schema.call?.token
        let professional = schema.professional

        var mainVC: MainViewController?
        switch callType {
        case .call:
            mainVC = ServiceLocator.getIncomingCall(userType: userType,
                                                    sessionId: sessionId,
                                                    callId: callId,
                                                    token: token,
                                                    professional: professional)
        case .video:
            mainVC = ServiceLocator.getIncomingVideo(userType: userType,
                                                     sessionId: sessionId,
                                                     callId: callId,
                                                     token: token,
                                                     professional: professional)
        }
        if let mainVC = mainVC {
            mainVC.modalPresentationStyle = .fullScreen
            currentViewController.present(mainVC, animated: true)
        }
    }

    private func showInProgressCall(currentViewController: UIViewController,
                                    by userType: UserType,
                                    by schema: VideoCallNotificationSchema) {

        guard let presenter = self.presenter else { return }

        let callId = schema.call?.uuid

        presenter.pickUp(for: userType, by: callId) { [weak self] result in
            switch result {
            case .success:
                if let viewController = self?.getInProgressCallViewController(userType: userType,
                                                                        schema: schema) {
                    viewController.modalPresentationStyle = .fullScreen
                    currentViewController.present(viewController, animated: true)
                }
            case .failure(let error):
                CoreLog.business.error("%@", error.localizedDescription)
            }
        }
    }
}

// MARK: - Private functions

extension VideoCallManager {

    private func processSchema(userInfo: [AnyHashable: Any]) -> VideoCallNotificationSchema? {
        if let hashTable: AnyHashable = userInfo["data"] as? AnyHashable {
            if let data = hashTable.description.data(using: .utf8) {
                if let notification: VideoCallNotificationSchema = try? JSONDecoder().decode(VideoCallNotificationSchema.self, from: data) {
                    return notification
                }
            }
        }
        return nil
    }

    private func getTopViewController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }

    private func getVideoCallManagerState(schema: VideoCallNotificationSchema) -> VideoCallManagerState {
        if getTopViewController() is MainViewController {
            guard let type = schema.type else { return .onAvailable }
            switch type {
            case .pickedUp:
                return .callPickedUp
            case .rejected:
                return .callRejected
            case .requested:
                return .callRequested
            }
        } else {
            let appState = UIApplication.shared.applicationState
            switch appState {
            case .active:
                return .onAvailable
            default:
                return .onBackground
            }
        }
    }

    private func getInProgressCallViewController(userType: UserType,
                                                 schema: VideoCallNotificationSchema) -> MainViewController? {
        guard let type = schema.call?.type else { return nil }

        let sessionId = schema.call?.sessionId
        let callId = schema.call?.uuid
        let token = schema.call?.token
        let customer = schema.customer
        let professional = schema.professional

        switch type {
        case .call:
            return ServiceLocator.getInProgressCall(
                userType: userType, sessionId: sessionId, callId: callId, token: token, customer: customer, professional: professional)
        case .video:
            return ServiceLocator.getInProgressVideo(
                userType: userType, sessionId: sessionId, callId: callId, token: token, customer: customer, professional: professional)
        }
    }
}
