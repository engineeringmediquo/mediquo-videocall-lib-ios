//
//  VideoCallNotificationSchema.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct VideoCallNotificationSchema: Codable {
    public enum CodingKeys: String, CodingKey {
        case module
        case type
        case call
        case professional
        case customer
        case permissions
    }

    public var module: String?
    public var type: ActionType?
    public var call: CallModel?
    public var professional: ProfessionalModel?
    public var customer: CustomerModel?
    public var permissions: Permission?
    public var enableReport: Bool?

    public init() {
        self.module = ""
        self.type = nil
        self.call = nil
        self.professional = nil
        self.customer = nil
        self.permissions = nil
        self.enableReport = false
    }

    public init(call: CallModel?) {
        self.init()
        self.call = call
    }

    public init(call: CallModel?, customer: CustomerModel?) {
        self.init(call: call)
        self.customer = customer
    }

    public init(call: CallModel?, professional: ProfessionalModel?) {
        self.init(call: call)
        self.professional = professional
    }

    public init(call: CallModel?, customer: CustomerModel?, enableReport: Bool?) {
        self.init(call: call, customer: customer)
        self.enableReport = enableReport
    }
}
