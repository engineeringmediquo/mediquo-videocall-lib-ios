//
//  CallModel.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct CallModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case uuid
        case roomId = "room_id"
        case type
        case sessionId = "session_id"
        case token
    }

    public let uuid: String?
    public let roomId: Int?
    public let type: CallType?
    public let sessionId: String?
    public let token: String?

    public init(uuid: String?, roomId: Int?, type: CallType?, sessionId: String?, token: String?) {
        self.uuid = uuid
        self.roomId = roomId
        self.type = type
        self.sessionId = sessionId
        self.token = token
    }
}
