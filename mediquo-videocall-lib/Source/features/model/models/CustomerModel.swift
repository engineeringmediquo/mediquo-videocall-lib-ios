//
//  CustomerModel.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 04/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct CustomerModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case hash
        case name
        case avatar
    }

    public let hash: String?
    public var name: String?
    public var avatar: String?

    public init() {
        self.hash = ""
        self.name = ""
    }

    public init(name: String?) {
        self.init()
        self.name = name
    }

    public init(name: String?, avatar: String?) {
        self.init(name: name)
        self.avatar = avatar
    }
}
