//
//  SessionModel.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct SessionModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case customerToken = "customer_token"
        case professionalToken = "professional_token"
        case sessionId = "session_id"
    }

    public let customerToken: String?
    public let professionalToken: String?
    public let sessionId: String?
}
