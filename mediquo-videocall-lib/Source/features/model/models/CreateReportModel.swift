//
//  CreateReportModel.swift
//  mediquo-videocall-lib
//
//  Created by Chus Clua on 03/07/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct CreateReportModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case uuid
        case time
        case name
    }

    public let uuid: String?
    public let time: String?
    public let name: String?

    public init(uuid: String?, time: String?, name: String?) {
        self.uuid = uuid
        self.time = time
        self.name = name
    }

}
