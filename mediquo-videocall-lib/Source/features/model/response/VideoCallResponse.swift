//
//  VideoCallResponse.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct VideoCallResponse: Codable {
    public enum CodingKeys: String, CodingKey {
        case call
        case session
    }

    public let call: CallModel?
    public let session: SessionModel?
}
