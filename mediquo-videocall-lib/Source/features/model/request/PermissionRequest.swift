//
//  PermissionRequest.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct PermissionRequest: Codable {
    public enum CodingKeys: String, CodingKey {
        case permissions
    }

    public let permissions: Permission
}

public struct Permission: Codable {
    public enum CodingKeys: String, CodingKey {
        case audio
        case video
    }

    public let audio: Bool
    public let video: Bool
}
