//
//  StorageManager.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 21/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public protocol StorageManagerProtocol {
    func save<T: Codable>(object: T, by key: String)
    func get<T: Codable>(by key: String) -> T?
    func delete(by key: String)
    func deleteAll()
}

public class StorageManager: StorageManagerProtocol {
    public func save<T: Codable>(object: T, by key: String) {
        guard let jsonData = try? JSONEncoder().encode(object), let jsonString = String(data: jsonData, encoding: .utf8) else { return }
        UserDefaults.standard.set(jsonString, forKey: key)
    }

    public func get<T: Codable>(by key: String) -> T? {
        guard let jsonString = UserDefaults.standard.string(forKey: key), let data = jsonString.data(using: .utf8) else { return nil }
        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            CoreLog.business.error("%@", error.localizedDescription)
            return nil
        }
    }

    public func delete(by key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }

    public func deleteAll() {
        if let domain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: domain)
        }
    }
}
