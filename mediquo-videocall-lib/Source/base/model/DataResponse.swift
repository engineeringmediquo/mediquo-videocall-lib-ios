//
//  DataResponse.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 27/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct DataResponse<Data: Codable>: Codable {
    public enum CodingKeys: String, CodingKey {
        case data
    }

    public let data: Data
}
