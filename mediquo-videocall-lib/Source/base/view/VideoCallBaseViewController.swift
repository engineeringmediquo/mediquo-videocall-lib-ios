//
//  BaseViewController.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

open class VideoCallBaseViewController: UIViewController {

    open override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForPushNotifications()
    }
}

extension VideoCallBaseViewController {
    private func registerForPushNotifications() {
        guard ServiceLocator.environmentRepository.get() != .testing else { return }
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge, .alert, .sound]) { (_: Bool, error: Error?) -> Void in
            if let error: Error = error {
                CoreLog.firebase.info("Authorization request error %@", error.localizedDescription)
                return
            }

            center.getNotificationSettings(completionHandler: { (settings: UNNotificationSettings) in
                guard settings.authorizationStatus == .authorized else {
                    CoreLog.firebase.info("Unauthorized remote notifications")
                    return
                }

                DispatchQueue.main.async {
                    CoreLog.firebase.info("Authorized remote notifications")
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }
    }
}
