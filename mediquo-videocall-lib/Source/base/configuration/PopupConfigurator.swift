//
//  PopupConfigurator.swift
//  mediquo-videocall-lib
//
//  Created by MediQuo Team iOS on 13/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

public enum PopupConfigurationType {
    case popupForAudioPermissionDenied(PopupForAudioConfiguration)
    case popupForVideoPermissionDenied(PopupForVideoConfiguration)
    case popupClientHasAudioPermissionDenied(PopupClientHasAudioPermissionDeniedConfiguration)
    case popupClientHasVideoPermissionDenied(PopupClientHasVideoPermissionDeniedConfiguration)
}

public protocol PopupConfiguration {
    var isHiddenCloseButton: Bool { get }
    var popupImage: UIImage { get }
    var popupTitle: String { get }
    var popupSubtitle: String? { get }
    var activeButtonTitle: String { get }
    var cancelButtonTitle: String? { get }
}

public struct PopupForAudioConfiguration: PopupConfiguration {
    public var isHiddenCloseButton = false
    public var popupImage: UIImage
    public var popupTitle: String
    public var popupSubtitle: String?
    public var activeButtonTitle: String
    public var cancelButtonTitle: String?
}

public struct PopupForVideoConfiguration: PopupConfiguration {
    public var isHiddenCloseButton = false
    public var popupImage: UIImage
    public var popupTitle: String
    public var popupSubtitle: String?
    public var activeButtonTitle: String
    public var cancelButtonTitle: String?
}

// MARK: - Popups for Professional

// swiftlint:disable type_name
public struct PopupClientHasAudioPermissionDeniedConfiguration: PopupConfiguration {
    public var isHiddenCloseButton = true
    public var popupImage: UIImage
    public var popupTitle: String
    public var popupSubtitle: String?
    public var activeButtonTitle: String
    public var cancelButtonTitle: String?
}

public struct PopupClientHasVideoPermissionDeniedConfiguration: PopupConfiguration {
    public var isHiddenCloseButton = true
    public var popupImage: UIImage
    public var popupTitle: String
    public var popupSubtitle: String?
    public var activeButtonTitle: String
    public var cancelButtonTitle: String?
}
