//
//  VideoCallConfiguration.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 04/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public enum VideoCallConfigurationType {
    // MARK: - Call
    case incomingCall(VideoCallIncomingCallConfiguration)
    case inProgressCall(VideoCallInProgressCallConfiguration)
    case outgoingVideo(VideoCallOutgoingVideoConfiguration)

    // MARK: - Video
    case incomingVideo(VideoCallIncomingVideoConfiguration)
    case inProgressVideo(VideoCallInProgressVideoConfiguration)
    case outgoingCall(VideoCallOutgoingCallConfiguration)
}

public protocol VideoCallConfiguration {
    var isHiddenFullScreenCameraView: Bool { get }
    var isHiddenLittleScreenCameraView: Bool { get }
    var isHiddenNameDoctorLabel: Bool { get }
    var isHiddenFirstLetterPatientView: Bool { get }
    var isHiddenProfessionalImageView: Bool { get }
    var isHiddenNamePatientOrProfessionalLabel: Bool { get }
    var isHiddenStatuCallLabel: Bool { get }
    var isHiddenMicroStackButton: Bool { get }
    var isHiddenCameraButton: Bool { get }
    var isHiddenSwapCameraButton: Bool { get }
    var isHiddenEndOrRejectCallButton: Bool { get }
    var isHiddenRejectCallButton: Bool { get }
    var isHiddenAcceptCallButton: Bool { get }
    var isHiddenRejectCallOrVideoCallLabel: Bool { get }
    var isHiddenAcceptCallOrVideoCallLabel: Bool { get }
}

// MARK: - Call

// CALL - PRO CALLING TO PATIENT ( VIEW ON MAIN-IOS)
public struct VideoCallIncomingCallConfiguration: VideoCallConfiguration {
    public let isHiddenFullScreenCameraView: Bool = false
    public let isHiddenLittleScreenCameraView: Bool = true
    public let isHiddenNameDoctorLabel: Bool = true
    public let isHiddenFirstLetterPatientView: Bool = true
    public let isHiddenProfessionalImageView: Bool = false
    public let isHiddenNamePatientOrProfessionalLabel: Bool = false
    public let isHiddenStatuCallLabel: Bool = false
    public let isHiddenMicroStackButton: Bool = true
    public let isHiddenCameraButton: Bool = true
    public let isHiddenSwapCameraButton: Bool = true
    public let isHiddenEndOrRejectCallButton: Bool = true
    public let isHiddenRejectCallButton: Bool = false
    public let isHiddenAcceptCallButton: Bool = false
    public let isHiddenRejectCallOrVideoCallLabel: Bool = false
    public let isHiddenAcceptCallOrVideoCallLabel: Bool = false
}

// CALL - PRO ON CALL TO PATIENT ( VIEW ON MAIN-IOS)
public struct VideoCallInProgressCallConfiguration: VideoCallConfiguration {
    public let isHiddenFullScreenCameraView: Bool = false
    public let isHiddenLittleScreenCameraView: Bool = true
    public let isHiddenNameDoctorLabel: Bool = true
    public let isHiddenProfessionalImageView: Bool = false
    public let isHiddenNamePatientOrProfessionalLabel: Bool = false
    public let isHiddenFirstLetterPatientView: Bool = false
    public let isHiddenStatuCallLabel: Bool = true
    public let isHiddenMicroStackButton: Bool = false
    public let isHiddenCameraButton: Bool = true
    public let isHiddenSwapCameraButton: Bool = false
    public let isHiddenEndOrRejectCallButton: Bool = false
    public let isHiddenRejectCallButton: Bool = true
    public let isHiddenAcceptCallButton: Bool = true
    public let isHiddenRejectCallOrVideoCallLabel: Bool = true
    public let isHiddenAcceptCallOrVideoCallLabel: Bool = true
}
// CALL - PRO CALLING TO PATIENT ( VIEW ON PRO-LIB)
public struct VideoCallOutgoingCallConfiguration: VideoCallConfiguration {
    public let isHiddenFullScreenCameraView: Bool = false
    public let isHiddenLittleScreenCameraView: Bool = true
    public let isHiddenNameDoctorLabel: Bool = true
    public let isHiddenProfessionalImageView: Bool = false
    public let isHiddenNamePatientOrProfessionalLabel: Bool = false
    public let isHiddenFirstLetterPatientView: Bool = false
    public let isHiddenStatuCallLabel: Bool = false
    public let isHiddenMicroStackButton: Bool = false
    public let isHiddenCameraButton: Bool = true
    public let isHiddenSwapCameraButton: Bool = false
    public let isHiddenEndOrRejectCallButton: Bool = false
    public let isHiddenRejectCallButton: Bool = true
    public let isHiddenAcceptCallButton: Bool = true
    public let isHiddenRejectCallOrVideoCallLabel: Bool = true
    public let isHiddenAcceptCallOrVideoCallLabel: Bool = true
}

// MARK: - Video

// VIDEOCALL - PRO VIDEOCAL TO PATIENT ( VIEW ON MAIN-IOS)
public struct VideoCallIncomingVideoConfiguration: VideoCallConfiguration {
    public let isHiddenFullScreenCameraView: Bool = false
    public let isHiddenLittleScreenCameraView: Bool = true
    public let isHiddenNameDoctorLabel: Bool = true
    public let isHiddenProfessionalImageView: Bool = false
    public let isHiddenNamePatientOrProfessionalLabel: Bool = false
    public let isHiddenFirstLetterPatientView: Bool = false
    public let isHiddenStatuCallLabel: Bool = false
    public let isHiddenMicroStackButton: Bool = true
    public let isHiddenCameraButton: Bool = true
    public let isHiddenSwapCameraButton: Bool = true
    public let isHiddenEndOrRejectCallButton: Bool = true
    public let isHiddenRejectCallButton: Bool = false
    public let isHiddenAcceptCallButton: Bool = false
    public let isHiddenRejectCallOrVideoCallLabel: Bool = false
    public let isHiddenAcceptCallOrVideoCallLabel: Bool = false
}

// VIDEOCALL - PRO  ON VIDEOCALL TO PATIENT ( VIEW ON MAIN-IOS)
public struct VideoCallInProgressVideoConfiguration: VideoCallConfiguration {
    public let isHiddenFullScreenCameraView: Bool = false
    public let isHiddenLittleScreenCameraView: Bool = true
    public let isHiddenNameDoctorLabel: Bool = true
    public let isHiddenProfessionalImageView: Bool = false
    public let isHiddenNamePatientOrProfessionalLabel: Bool = false
    public let isHiddenFirstLetterPatientView: Bool = true
    public let isHiddenStatuCallLabel: Bool = true
    public let isHiddenMicroStackButton: Bool = false
    public let isHiddenCameraButton: Bool = false
    public let isHiddenSwapCameraButton: Bool = false
    public let isHiddenEndOrRejectCallButton: Bool = false
    public let isHiddenRejectCallButton: Bool = true
    public let isHiddenAcceptCallButton: Bool = true
    public let isHiddenRejectCallOrVideoCallLabel: Bool = true
    public let isHiddenAcceptCallOrVideoCallLabel: Bool = true
}

// VIDEOCALL - PRO  ON VIDEOCALL TO PATIENT ( VIEW ON PRO-LIB)
public struct VideoCallOutgoingVideoConfiguration: VideoCallConfiguration {
    public let isHiddenFullScreenCameraView: Bool = false
    public let isHiddenLittleScreenCameraView: Bool = true
    public let isHiddenNameDoctorLabel: Bool = true
    public let isHiddenProfessionalImageView: Bool = false
    public let isHiddenNamePatientOrProfessionalLabel: Bool = false
    public let isHiddenFirstLetterPatientView: Bool = false
    public let isHiddenStatuCallLabel: Bool = false
    public let isHiddenMicroStackButton: Bool = false
    public let isHiddenCameraButton: Bool = false
    public let isHiddenSwapCameraButton: Bool = false
    public let isHiddenEndOrRejectCallButton: Bool = false
    public let isHiddenRejectCallButton: Bool = true
    public let isHiddenAcceptCallButton: Bool = true
    public let isHiddenRejectCallOrVideoCallLabel: Bool = true
    public let isHiddenAcceptCallOrVideoCallLabel: Bool = true
}
