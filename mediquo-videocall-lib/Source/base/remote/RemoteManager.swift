// Copyright © 2020 Medipremium S.L. All rights reserved.

import Alamofire
import UIKit

private struct AuthTags {
    static let authorization = "Authorization"
}

public typealias HTTPHeaders = [String: String]
public typealias RemoteCompletionTypeAlias<Response: Codable> = (Swift.Result<Response, Error>) -> Void

public protocol RemoteManagerProtocol: AnyObject {
    func post<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func get<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func put<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func delete<Response: Codable>(_ endpoint: String, headers: [String: String], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>)
}

extension RemoteManagerProtocol {
    func post<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String? = nil, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.post(endpoint, headers: headers, parameters: parameters, token: token, completion: completion)
    }

    func get<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String? = nil, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.get(endpoint, headers: headers, parameters: parameters, token: token, completion: completion)
    }

    func put<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String? = nil, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.put(endpoint, headers: headers, parameters: parameters, token: token, completion: completion)
    }

    func delete<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String? = nil, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.delete(endpoint, headers: headers, parameters: parameters, token: token, completion: completion)
    }
}

public class RemoteManager: RemoteManagerProtocol {
    private var configuration: RemoteConfiguration
    private let baseUrl: String

    public init(_ configuration: RemoteConfiguration) {
        self.configuration = configuration
        self.baseUrl = configuration.baseUrl
    }

    deinit {
        self.configuration.session.session.invalidateAndCancel()
    }

    public func post<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers, token: token, completion: completion)
    }

    public func get<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers, token: token, completion: completion)
    }

    public func put<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers, token: token, completion: completion)
    }

    public func delete<Response: Codable>(_ endpoint: String, headers: [String: String] = [:], parameters: Codable?, token: String?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers, token: token, completion: completion)
    }

    // swiftlint:disable cyclomatic_complexity function_body_length
    internal func request<Response: Codable>(_ endpoint: String,
                                             method: HTTPMethod,
                                             parameters: Codable?,
                                             encoding: ParameterEncoding = JSONEncoding.default,
                                             headers: [String: String] = [:],
                                             token: String? = nil,
                                             completion: @escaping RemoteCompletionTypeAlias<Response>) {
        guard let url = URL(string: self.baseUrl + endpoint) else { return }

        let request = parameters?.dictionary
        let newHeaders = self.build(headers: headers, with: token)

        self.configuration.session
            .request(url, method: method, parameters: request, encoding: encoding, headers: newHeaders)
            .validate(contentType: ["application/json"])
            .responseData { [weak self] dataResponse in
                guard let self = self else { return }
                if let data = dataResponse.data, let stringResponse: String = String(data: data, encoding: .utf8) {
                    CoreLog.remote.debug("[RemoteManager] Request: %@", url.description)
                    CoreLog.remote.debug("[RemoteManager] Response: %@", stringResponse)
                }
                if let response = dataResponse.response {
                    let statusCode = response.statusCode
                    guard statusCode >= 200, statusCode < 300 else {
                        completion(.failure(VideoCallError.remoteError(.httpUrlResponse(response))))
                        return
                    }

                    switch dataResponse.result {
                    case let .success(data):
                        do {
                            if let response: Response = try self.convertFrom(data: data) {
                                completion(.success(response))
                            } else {
                                completion(.failure(VideoCallError.remoteError(.reason("Wrong response received"))))
                            }
                        } catch {
                            completion(.failure(VideoCallError.remoteError(.reason(error.localizedDescription))))
                        }
                    case let .failure(error):
                        completion(.failure(VideoCallError.remoteError(.reason(error.localizedDescription))))
                    }
                } else {
                    completion(.failure(VideoCallError.remoteError(.reason(dataResponse.error?.localizedDescription ?? "Networking error connection"))))
                }
            }
    }

    private func build(headers: HTTPHeaders, with token: String?) -> HTTPHeaders {
        var newHeaders = headers
        newHeaders["Accept"] = "application/json"
        newHeaders["Content-Type"] = "application/json"

        let isSdk = ServiceLocator.isSdk
        if isSdk {
            newHeaders["X-API-Key"] = ServiceLocator.apiKeySdk
        } else {
            newHeaders["apiKey"] = "qJbB9tEqtdG4MsVb"
        }

        if let customUserAgent = self.customUserAgent() {
            newHeaders["User-Agent"] = customUserAgent
        }

        newHeaders.merge(self.configuration.defaultHeaders) { current, _ in current }

        guard let token = token else { return newHeaders }
        newHeaders[AuthTags.authorization] = "Bearer \(token)"

        return newHeaders
    }

    private func convertFrom<Response: Codable>(data: Data) throws -> Response? {
        return try? JSONDecoder().decode(Response.self, from: data)
    }

    private func customUserAgent() -> String? {
        guard let appVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String,
              let appBundleVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String else { return nil }
        return ["mediquopatient/", appVersion, "(\(appBundleVersion))", " ios/", UIDevice.current.systemVersion, " ", Device.type().rawValue, "/", Device.version().rawValue].joined()
    }
}
