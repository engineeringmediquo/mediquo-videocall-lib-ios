// Copyright © 2020 Medipremium S.L. All rights reserved.

import Alamofire
import UIKit

public struct RemoteConfiguration {
    let baseUrl: String
    public let defaultHeaders: HTTPHeaders
    public let timeout: Int
    public let retryAttempts: Int

    public init(baseUrl: String, defaultHeaders: HTTPHeaders = [:], timeout: Int = 30, retryAttempts: Int = 1) {
        self.baseUrl = baseUrl
        self.defaultHeaders = defaultHeaders
        self.timeout = timeout
        self.retryAttempts = retryAttempts
    }

    public let session = SessionManager(serverTrustPolicyManager: ServerTrustPolicyManager(policies: [:]))
}
