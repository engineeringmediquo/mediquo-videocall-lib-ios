//
//  UIView+Extension.swift
//  mediquo-videocall-lib
//
//  Created by JORDI GALLEN RENAU on 01/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func circularShape() {
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
        self.clipsToBounds = true
    }

    func removeAllSubViews() {
        for subView in self.subviews {
            subView.removeFromSuperview()
        }
    }

    func cornerRounded(_ cornerRadius: CGFloat = 10) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }

    func fadeIn(withDuration duration: TimeInterval = 3, alpha: CGFloat) {
        UIView.self.animate(withDuration: duration, animations: {
            self.alpha = alpha
        })
    }

    func fadeOut(withDuration duration: TimeInterval = 2, alpha: CGFloat) {
       UIView.self.animate(withDuration: duration, animations: {
            self.alpha = 0
        })
    }

    func addTopShadow(color: UIColor = .black, opacity: Float = 0.8, radius: CGFloat = 5.0) {
        self.addShadow(offset: CGSize(width: 0, height: 96), color: color, opacity: opacity, radius: radius)
    }

    internal func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }

    func removeAddTopShadow() {
        self.layer.shadowColor = UIColor.white.cgColor
    }

    func addBlurToView() {
        let blurEffect: UIBlurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect )
        blurredEffectView.frame = self.bounds
        blurredEffectView.alpha = 0.8
        blurredEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurredEffectView)
    }

    func removeBlurFromView() {
        for subview in self.subviews where subview is UIVisualEffectView {
            subview.removeFromSuperview()
        }
    }
}
