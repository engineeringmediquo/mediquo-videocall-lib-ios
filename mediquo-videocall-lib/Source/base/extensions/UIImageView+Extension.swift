//
//  UIImageView.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 19/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//
import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
