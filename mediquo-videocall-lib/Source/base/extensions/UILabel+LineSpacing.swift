//
//  UILabel+LineSpacing.swift
//  mediquo-videocall-lib
//
//  Created by MediQuo Team iOS on 12/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

extension UILabel {
    func addLineSpacingTo(text: String, font: UIFont, color: UIColor, lineSpacing: CGFloat = 5, alignment: NSTextAlignment = .center) {
        let attrs: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]

        let attributedString = NSMutableAttributedString(string: text, attributes: attrs)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = alignment
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
}
