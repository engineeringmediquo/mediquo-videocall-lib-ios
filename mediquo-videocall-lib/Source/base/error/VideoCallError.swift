//
//  VideoCallError.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 04/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public enum VideoCallError: Error {
    case authError(AuthError)
    case repositoryError(RepositoryError)
    case remoteError(RemoteError)
    case ui(UIError)

    public enum AuthError {
        case generic(Error)
        case token
        case reason(String)
        case badURL

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            case let .generic(error):
                return "Generic: \(error.localizedDescription)"
            default:
                return String(describing: self)
            }
        }
    }

    public enum RepositoryError {
        case noResponseData
        case localStorageUpToDate
        case reason(String)

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            default:
                return String(describing: self)
            }
        }
    }

    public enum RemoteError {
        case httpUrlResponse(HTTPURLResponse)
        case code(Int)
        case reason(String)

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            default:
                return String(describing: self)
            }
        }
    }

    public enum UIError {
        case cantLoad
        case notResponds
        case notFound
        case reason(String)

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            default:
                return String(describing: self)
            }
        }
    }

    var description: String {
        switch self {
        case let .authError(error):
            return "AuthError - " + error.description
        case let .repositoryError(error):
            return "RepositoryError - " + error.description
        case let .remoteError(error):
            return "RemoteError - " + error.description
        case let .ui(error):
            return "UIError - " + error.description
        }
    }
}

extension VideoCallError: Equatable {
    public static func == (lhs: VideoCallError, rhs: VideoCallError) -> Bool {
        return lhs.description == rhs.description
    }
}
