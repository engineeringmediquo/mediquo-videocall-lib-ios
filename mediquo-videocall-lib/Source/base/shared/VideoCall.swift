//
//  VideoCall.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public final class VideoCall {
    public static func initLib(to environment: Environment = .production, with token: String, and localizable: LocalizableVC? = nil) {
        self.set(isSdk: false)
        self.set(apiKeySdk: nil)
        self.set(environment: environment)
        self.set(token: token)

        guard let localizable = localizable else { return }
        ServiceLocator.localizable = localizable
    }

    public static func initLibForSdk(to environment: Environment = .production, apiKey: String, token: String, and localizable: LocalizableVC? = nil) {
        self.set(isSdk: true)
        self.set(apiKeySdk: apiKey)
        self.set(environment: environment)
        self.set(token: token)

        guard let localizable = localizable else { return }
        ServiceLocator.localizable = localizable
    }

    public static func getVideoCallManager() throws -> VideoCallManagerProtocol {
        guard let token = ServiceLocator.userRepository.getToken(), !token.isEmpty else { throw VideoCallError.authError(.token) }
        return ServiceLocator.videoCallManager()
    }

    // MARK: - In progress current status control

    static var isInProgress: Bool = ServiceLocator.isInProgress
}

extension VideoCall {
    private class func set(environment: Environment) {
        ServiceLocator.environmentRepository.set(environment: environment)
    }

    private class func set(token: String) {
        ServiceLocator.userRepository.set(token: token)
    }

    private class func set(isSdk: Bool) {
        ServiceLocator.isSdk = isSdk
    }

    private class func set(apiKeySdk: String?) {
        ServiceLocator.apiKeySdk = apiKeySdk
    }
}
