//
//  Environment.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

#if DEV
    let environment: Environment = .develop
#elseif PROD
    let environment: Environment = .production
#endif

public enum Environment: String {
    case develop
    case testing
    case production
}
