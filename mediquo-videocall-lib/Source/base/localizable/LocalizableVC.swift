//
//  LocalizableVC.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 15/06/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public struct LocalizableVC {
    public var incomingCall: String = L10n.Shared.incomingCall
    public var incomingVideoCall: String = L10n.Shared.incomingVideoCall
    public var calling: String = L10n.Shared.calling
    public var videoCalling: String = L10n.Shared.videoCalling
    public var rejectedCall: String = L10n.Shared.rejectedCall
    public var rejectedVideoCall: String = L10n.Shared.rejectedVideoCall
    public var endedVideoCall: String = L10n.Shared.endedCall
    public var cameraAndMicrophoneDeactivated: String = L10n.Shared.cameraAndMicrophoneDeactivated
    public var cameraDeactivated: String = L10n.Shared.cameraDeactivated
    public var microphoneDesactivated: String = L10n.Shared.microphoneDesactivated
    public var speakerActivated: String = L10n.Shared.speakerActivated
    public var reconnecting: String = L10n.Shared.reconnecting
    public var requestingChangeToVideoCall: String = L10n.Shared.requestingChangeToVideoCall
    public var cancel: String = L10n.Shared.cancel
    public var refuse: String = L10n.Shared.refuse
    public var accept: String = L10n.Shared.accept
    public var applicationCanceled: String = L10n.Shared.applicationCanceled
    public var isAskingYouChangeToVideoall: String = L10n.Shared.isAskingYouChangeToVideoall
    public var ok: String = L10n.Shared.ok
    public var error: String = L10n.Shared.error

    public var microProLabel: String = L10n.Mainviewcontoller.CentralToast.Professional.Micro.label
    public var camaraProLabel: String = L10n.Mainviewcontoller.CentralToast.Professional.Camara.label
    public var microAndCamaraProLabel: String = L10n.Mainviewcontoller.CentralToast.Professional.MicroAndCamara.label

    public var microCustomerLabel: String = L10n.Mainviewcontoller.CentralToast.Customer.Micro.label
    public var camaraCustomerLabel: String = L10n.Mainviewcontoller.CentralToast.Customer.Camara.label
    public var microAndCamaraCustomerLabel: String = L10n.Mainviewcontoller.CentralToast.Customer.MicroAndCamara.label

    public var permissionAudioTitle: String = L10n.Popup.Permission.Audio.title
    public var permissionAudioSubTitle: String = L10n.Popup.Permission.Audio.subtitle
    public var permissionAudioButton: String = L10n.Popup.Permission.Audio.Action.button

    public var permissionVideoTitle: String = L10n.Popup.Permission.Video.title
    public var permissionVideoSubTitle: String = L10n.Popup.Permission.Video.subtitle
    public var permissionVideoButton: String = L10n.Popup.Permission.Video.Action.button

    public var permissionAudioDeniedTitle: String = L10n.Popup.Professional.Client.Permission.Audio.Denied.title
    public var permissionAudioDeniedSubTitle: String = L10n.Popup.Professional.Client.Permission.Audio.Denied.subtitle
    public var permissionAudioDeniedButton: String = L10n.Popup.Professional.Client.Permission.Audio.Denied.Action.button

    public var permissionVideoDeniedTitle: String = L10n.Popup.Professional.Client.Permission.Video.Denied.title
    public var permissionVideoDeniedSubTitle: String = L10n.Popup.Professional.Client.Permission.Video.Denied.subtitle
    public var permissionVideoDeniedButton: String = L10n.Popup.Professional.Client.Permission.Video.Denied.Action.button

    public var createReportMessage: String = L10n.Createreportcontroller.Message.label
    public var createReportExitButton: String = L10n.Createreportcontroller.Exit.button
    public var createReportCreateButton: String = L10n.Createreportcontroller.Create.button

    public var popupBusyTitle: String = L10n.Popup.Busy.title
    public var popupBusySubtitle: String = L10n.Popup.Busy.subtitle
    public var popupBusyButton: String = L10n.Popup.Busy.button

    public var ratingTitle: String = L10n.Videocall.Rating.Call.ended
    public var ratingMessage: String = L10n.Videocall.Rating.message
    public var ratingGood: String = L10n.Videocall.Rating.good
    public var ratingImprovable: String = L10n.Videocall.Rating.improvable

    public init() { }

    //swiftlint:disable function_body_length
    public init(incomingCall: String,
                incomingVideoCall: String,
                calling: String,
                videoCalling: String,
                rejectedCall: String,
                rejectedVideoCall: String,
                endedVideoCall: String,
                cameraAndMicrophoneDeactivated: String,
                cameraDeactivated: String,
                microphoneDesactivated: String,
                speakerActivated: String,
                reconnecting: String,
                requestingChangeToVideoCall: String,
                cancel: String,
                refuse: String,
                accept: String,
                applicationCanceled: String,
                isAskingYouChangeToVideoall: String,
                ok: String,
                error: String,
                microProLabel: String,
                camaraProLabel: String,
                microAndCamaraProLabel: String,
                microCustomerLabel: String,
                camaraCustomerLabel: String,
                microAndCamaraCustomerLabel: String,
                permissionAudioTitle: String,
                permissionAudioSubTitle: String,
                permissionAudioButton: String,
                permissionVideoTitle: String,
                permissionVideoSubTitle: String,
                permissionVideoButton: String,
                permissionAudioDeniedTitle: String,
                permissionAudioDeniedSubTitle: String,
                permissionAudioDeniedButton: String,
                permissionVideoDeniedTitle: String,
                permissionVideoDeniedSubTitle: String,
                permissionVideoDeniedButton: String,
                createReportMessage: String,
                createReportExitButton: String,
                createReportCreateButton: String,
                popupBusyTitle: String,
                popupBusySubtitle: String,
                popupBusyButton: String,
                ratingTitle: String,
                ratingMessage: String,
                ratingGood: String,
                ratingImprovable: String) {
        self.incomingCall = incomingCall
        self.incomingVideoCall = incomingVideoCall
        self.calling = calling
        self.videoCalling = videoCalling
        self.rejectedCall = rejectedCall
        self.rejectedVideoCall = rejectedVideoCall
        self.endedVideoCall = endedVideoCall
        self.cameraAndMicrophoneDeactivated = cameraAndMicrophoneDeactivated
        self.cameraDeactivated = cameraDeactivated
        self.microphoneDesactivated = microphoneDesactivated
        self.speakerActivated = speakerActivated
        self.reconnecting = reconnecting
        self.requestingChangeToVideoCall = requestingChangeToVideoCall
        self.applicationCanceled = applicationCanceled
        self.refuse = refuse
        self.accept = accept
        self.applicationCanceled = applicationCanceled
        self.isAskingYouChangeToVideoall = isAskingYouChangeToVideoall
        self.ok = ok
        self.error = error
        self.microProLabel = microProLabel
        self.camaraProLabel = camaraProLabel
        self.microAndCamaraProLabel = microAndCamaraProLabel
        self.microCustomerLabel = microCustomerLabel
        self.camaraCustomerLabel = camaraCustomerLabel
        self.microAndCamaraCustomerLabel = microAndCamaraCustomerLabel
        self.permissionAudioTitle = permissionAudioTitle
        self.permissionAudioSubTitle = permissionAudioSubTitle
        self.permissionAudioButton = permissionAudioButton
        self.permissionVideoTitle = permissionVideoTitle
        self.permissionVideoSubTitle = permissionVideoSubTitle
        self.permissionVideoButton = permissionVideoButton
        self.permissionAudioDeniedTitle = permissionAudioDeniedTitle
        self.permissionAudioDeniedSubTitle = permissionAudioDeniedSubTitle
        self.permissionAudioDeniedButton = permissionAudioDeniedButton
        self.permissionVideoDeniedTitle = permissionVideoDeniedTitle
        self.permissionVideoDeniedSubTitle = permissionVideoDeniedSubTitle
        self.permissionVideoDeniedButton = permissionVideoDeniedButton
        self.createReportMessage = createReportMessage
        self.createReportExitButton = createReportExitButton
        self.createReportCreateButton = createReportCreateButton
        self.popupBusyTitle = popupBusyTitle
        self.popupBusySubtitle = popupBusySubtitle
        self.popupBusyButton = popupBusyButton
        self.ratingTitle = ratingTitle
        self.ratingMessage = ratingMessage
        self.ratingGood = ratingGood
        self.ratingImprovable = ratingImprovable
    }
}
