//
//  MediaController.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 07/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import AVFoundation

public enum SoundType {
    case ring
    case wait
}

public protocol MediaControllerProtocol: AnyObject {
    func playSound()
    func stopSound()
}

public class MediaController: MediaControllerProtocol {
    private let soundType: SoundType
    private var player: AVAudioPlayer?
    private var soundTimer: Timer?

    public init(soundType: SoundType) {
        self.soundType = soundType
    }

    public func playSound() {
        guard let dataSound = self.getDataSound(), let url = Bundle.main.url(forResource: dataSound.name, withExtension: dataSound.ext) else { return }

        if let timer = self.soundTimer, timer.isValid { return }

        self.soundTimer = Timer.scheduledTimer(withTimeInterval: dataSound.time, repeats: true) { _ in
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)

                self.player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
                self.player?.play()
            } catch {
                CoreLog.business.error("%@", error.localizedDescription)
            }
        }
        self.soundTimer?.fire()
    }

    public func stopSound() {
        self.soundTimer?.invalidate()
        self.player?.stop()
    }
}

extension MediaController {
    private func getDataSound() -> SoundModel? {
        switch self.soundType {
        case .ring:
            return SoundRing.model
        case .wait:
            return SoundWait.model
        }
    }
}

// MARK: - Sound model

private protocol SoundModelProtocol: AnyObject {
    static var model: SoundModel { get }
}

private class SoundRing: SoundModelProtocol {
    static var model: SoundModel = SoundModel(name: "ring", ext: "caf", time: 7)
}

private class SoundWait: SoundModelProtocol {
    static var model: SoundModel = SoundModel(name: "wait", ext: "caf", time: 3)
}

private struct SoundModel {
    let name: String
    let ext: String
    let time: TimeInterval
}
