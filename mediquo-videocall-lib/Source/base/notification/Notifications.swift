//
//  Notifications.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 11/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public extension Notification.Name {
    struct MediQuoVideoCall {
        public struct Call {
            public static let View = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.view")
            public static let PickUp = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.pick.up")
            public static let End = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.end")
            public static let Reject = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.reject")
            public static let ToggleAudio = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.toggle.audio")
            public static let ToggleVideo = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.toggle.video")
            public static let SwapCamera = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.swap.camera")
            public static let PermissionAudioRequest = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.permission.audio.request")
            public static let PermissionAudioAccept = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call..permission.audio.accept")
            public static let PermissionVideoRequest = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.permission.video.request")
            public static let PermissionVideoAccept = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.call.permission.video.accept")
        }
        public struct PermissionDenied {
            public static let audio = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.permission.denied.audio")
            public static let video = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.permission.denied.video")
        }
        public struct Report {
            public static let dismiss = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.report.dismiss")
            public static let create = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.report.create")
        }
        public struct Sdk {
            public struct Call {
                public static let Started = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.sdk.call.started")
                public static let Ended = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.sdk.call.ended")
            }
            public struct VideoCall {
                public static let Started = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.sdk.videoCall.started")
                public static let Ended = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.sdk.videoCall.ended")
            }
        }
        public struct Rating {
            public static let View = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.rating.view")
            public static let Close = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.rating.close")
            public static let Good = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.rating.good")
            public static let Improvable = Notification.Name(rawValue: "com.mediquo.videcall.notification.name.rating.improvable")
        }
    }

    struct MediQuoVideoCallKey {
        public static let CallType = "com.mediquo.videocall.notification.key.type"
        public static let CallId = "com.mediquo.videocall.notification.key.id"
    }
}
