//
//  ServiceLocator.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 23/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

class ServiceLocator {

    private init() { }

    // MARK: - Common

    internal static var storage: StorageManagerProtocol = StorageManager()

    internal static var localizable: LocalizableVC = LocalizableVC()

    // MARK: Call / VideoCall current status

    internal static var isInProgress: Bool = false

    // MARK: - Sdk

    internal static var isSdk: Bool = false
    internal static var apiKeySdk: String?
}
