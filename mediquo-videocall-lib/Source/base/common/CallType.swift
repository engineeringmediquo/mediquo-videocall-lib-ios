//
//  CallType.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public enum CallType: String, Codable {
    case call
    case video
}
