//
//  PopupManagerViewController.swift
//  mediquo-videocall-lib
//
//  Created by MediQuo Team iOS on 11/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

protocol PopupManagerViewControllerProtocol: AnyObject {
    func closeModal()
    func performAction()
    func cancelAction()
}

extension PopupManagerViewControllerProtocol {
    func closeModal() {
        guard let viewController = (self as? UIViewController) else { return }
        viewController.dismiss(animated: true)
    }

    public func cancelAction() {
        guard let viewController = (self as? UIViewController) else { return }
        viewController.dismiss(animated: true)
    }
}

public class PopupManagerViewController: VideoCallBaseViewController {
    @IBOutlet internal var backgrounView: UIView! {
        didSet {
            self.backgrounView.cornerRounded()
        }
    }
    @IBOutlet internal var closeButton: UIButton! {
        didSet {
            self.closeButton.isHidden = self.isHiddenCloseButton
            self.closeButton.setImage(Asset.icClose.image, for: .normal)
        }
    }
    @IBOutlet internal var imageView: UIImageView! {
        didSet {
            self.imageView.image = self.popupImage
        }
    }
    @IBOutlet internal var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = self.popupTitle
            self.titleLabel.textColor = ColorName.indigoBlue.color
            self.titleLabel.font = UIFont(name: FontFamily.GothamRounded.medium.name, size: 18)
        }
    }
    @IBOutlet internal var subtitlelabel: UILabel! {
        didSet {
            guard let subtitle = self.popupSubtitle else {
                    self.subtitlelabel.isHidden = true
                    return
            }
            self.subtitlelabel.addLineSpacingTo(text: subtitle,
                                                font: FontFamily.GothamRounded.book.font(size: 14),
                                                color: ColorName.lightGrey.color)
        }
    }
    @IBOutlet internal var performActionButton: UIButton! {
        didSet {
            guard let buttonTitle = self.activeButtonTitle else { return }
            self.setButtonStylesFor(button: self.performActionButton,
                                    text: buttonTitle,
                                    font: FontFamily.GothamRounded.medium.font(size: 18),
                                    textColor: .white,
                                    background: ColorName.indigoBlue.color)
            self.performActionButton.layer.cornerRadius = self.cornerRadius
            self.performActionButton.clipsToBounds = true
        }
    }
    @IBOutlet internal var cancelActionButton: UIButton! {
        didSet {
            guard let cancelButtonTitle = self.cancelButtonTitle else {
                    self.cancelActionButton.isHidden = true
                    return
            }
            self.setButtonStylesFor(button: self.cancelActionButton,
                                    text: cancelButtonTitle,
                                    font: FontFamily.GothamRounded.medium.font(size: 18),
                                    textColor: ColorName.indigoBlue.color,
                                    background: .white)
        }
    }

    internal var configuration: PopupConfigurationType? {
        didSet {
            self.loadScene()
        }
    }

    internal var configScreen: PopupConfiguration? {
        didSet {
            guard let configScreen = self.configScreen else { return }
            self.refreshSceneWith(configScreen)
        }
    }

    // MARK: - Private funcs

    private func setButtonStylesFor(button: UIButton, text: String, font: UIFont, textColor: UIColor, background: UIColor) {
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = font
        button.titleLabel?.textColor = textColor
        button.backgroundColor = background
    }

    private func refreshSceneWith(_ configuration: PopupConfiguration) {
        isHiddenCloseButton = configuration.isHiddenCloseButton
        popupImage = configuration.popupImage
        popupTitle = configuration.popupTitle
        popupSubtitle = configuration.popupSubtitle
        activeButtonTitle = configuration.activeButtonTitle
        cancelButtonTitle = configuration.cancelButtonTitle
    }

    private let cornerRadius: CGFloat = 10
    var isHiddenCloseButton = false
    var popupImage: UIImage?
    var popupTitle: String?
    var popupSubtitle: String?
    var activeButtonTitle: String?
    private var cancelButtonTitle: String?
    weak var delegate: PopupManagerViewControllerProtocol?

    @IBAction func closeButton(_ sender: Any) {
        self.delegate?.closeModal()
    }

    @IBAction func performActionButton(_ sender: Any) {
        self.delegate?.performAction()
    }

    @IBAction func cancelActionButton(_ sender: Any) {
        self.delegate?.cancelAction()
    }
}

// MARK: Control scenarios

extension PopupManagerViewController {
    private func loadScene() {
        switch self.configuration {
        case .popupForAudioPermissionDenied(let configScreen):
            self.configScreen = configScreen
        case .popupForVideoPermissionDenied(let configScreen):
            self.configScreen = configScreen
        case .popupClientHasAudioPermissionDenied(let configScreen):
            self.configScreen = configScreen
        case .popupClientHasVideoPermissionDenied(let configScreen):
            self.configScreen = configScreen
        default:
            break
        }
    }
}
