//
//  PermissionWrapper.swift
//  mediquo-videocall-lib
//
//  Created by MediQuo Team iOS on 08/05/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import AVFoundation
import UIKit

protocol PermissionWrapperProtocol: AnyObject {
    func checkMicrophoneAvailable(completion: @escaping ((Bool) -> Void))
    func checkVideoCameraAvailable(completion: @escaping ((Bool) -> Void))
}

internal enum PermissionErrorType: Error {
    case audioPermissionDenied
    case videoPermissionDenied
}

public class PermissionWrapper: PermissionWrapperProtocol {
    func checkMicrophoneAvailable(completion: @escaping ((Bool) -> Void)) {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .granted:
            completion(true)
        case .denied:
            completion(false)
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission { (granted: Bool) -> Void in
                completion(granted)
            }
        @unknown default:
            completion(false)
        }
    }

    func checkVideoCameraAvailable(completion: @escaping ((Bool) -> Void)) {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)

        switch cameraAuthorizationStatus {
        case .denied, .restricted:
            completion(false)
        case .authorized:
            completion(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                completion(granted)
            }
        @unknown default:
            completion(false)
        }
    }
}
