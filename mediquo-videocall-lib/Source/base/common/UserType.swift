//
//  UserType.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 28/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public enum UserType {
    case patient
    case professional
}
