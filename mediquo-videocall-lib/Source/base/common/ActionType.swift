//
//  ActionType.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 30/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

public enum ActionType: String, Codable {
    case pickedUp = "call_picked_up"
    case requested = "call_requested"
    case rejected = "call_rejected"
}
