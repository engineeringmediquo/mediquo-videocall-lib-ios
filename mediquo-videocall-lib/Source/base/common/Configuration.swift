//
//  Configuration.swift
//  mediquo-videocall-lib
//
//  Created by David Martin on 27/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

enum Configuration {
    static let apiKey = "46655102"
    static let callRetries = 1
}
