// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum Createreportcontroller {
    internal enum Create {
      /// Generar informe
      internal static let button = L10n.tr("LocalizableVC", "createreportcontroller.create.button")
    }
    internal enum Exit {
      /// Salir
      internal static let button = L10n.tr("LocalizableVC", "createreportcontroller.exit.button")
    }
    internal enum Message {
      /// Te recomendamos generar un informe de la consulta para poder guardarlo en el historial del paciente
      internal static let label = L10n.tr("LocalizableVC", "createreportcontroller.message.label")
    }
  }

  internal enum Mainviewcontoller {
    internal enum CentralToast {
      internal enum Customer {
        internal enum Camara {
          /// El profesional ha deshabilitado su cámara
          internal static let label = L10n.tr("LocalizableVC", "mainviewcontoller.centralToast.customer.camara.label")
        }
        internal enum Micro {
          /// El profesional ha deshabilitado su micrófono
          internal static let label = L10n.tr("LocalizableVC", "mainviewcontoller.centralToast.customer.micro.label")
        }
        internal enum MicroAndCamara {
          /// El profesional ha deshabilitado su cámara y micrófono
          internal static let label = L10n.tr("LocalizableVC", "mainviewcontoller.centralToast.customer.microAndCamara.label")
        }
      }
      internal enum Professional {
        internal enum Camara {
          /// El paciente ha deshabilitado su cámara
          internal static let label = L10n.tr("LocalizableVC", "mainviewcontoller.centralToast.professional.camara.label")
        }
        internal enum Micro {
          /// El paciente ha deshabilitado su micrófono
          internal static let label = L10n.tr("LocalizableVC", "mainviewcontoller.centralToast.professional.micro.label")
        }
        internal enum MicroAndCamara {
          /// El paciente ha deshabilitado su cámara y micrófono
          internal static let label = L10n.tr("LocalizableVC", "mainviewcontoller.centralToast.professional.microAndCamara.label")
        }
      }
    }
  }

  internal enum Popup {
    internal enum Busy {
      /// De acuerdo
      internal static let button = L10n.tr("LocalizableVC", "popup.busy.button")
      /// Puedes intentar llamar a tu contacto en otro momento
      internal static let subtitle = L10n.tr("LocalizableVC", "popup.busy.subtitle")
      /// El contacto está comunicando
      internal static let title = L10n.tr("LocalizableVC", "popup.busy.title")
    }
    internal enum Permission {
      internal enum Audio {
        /// Para poder usar esta funcionalidad activa los permisos de micro en los ajustes de tu teléfono
        internal static let subtitle = L10n.tr("LocalizableVC", "popup.permission.audio.subtitle")
        /// Llamada rechazada por falta de permisos
        internal static let title = L10n.tr("LocalizableVC", "popup.permission.audio.title")
        internal enum Action {
          /// Ir a mis ajustes
          internal static let button = L10n.tr("LocalizableVC", "popup.permission.audio.action.button")
        }
      }
      internal enum Video {
        /// Para poder usar esta funcionalidad activa los permisos de micro y cámara en los ajustes de tu teléfono
        internal static let subtitle = L10n.tr("LocalizableVC", "popup.permission.video.subtitle")
        /// Videollamada rechazada por falta de permisos
        internal static let title = L10n.tr("LocalizableVC", "popup.permission.video.title")
        internal enum Action {
          /// Ir a mis ajustes
          internal static let button = L10n.tr("LocalizableVC", "popup.permission.video.action.button")
        }
      }
    }
    internal enum Professional {
      internal enum Client {
        internal enum Permission {
          internal enum Audio {
            internal enum Denied {
              /// Puedes recordarle que se los active utilizando el chat
              internal static let subtitle = L10n.tr("LocalizableVC", "popup.professional.client.permission.audio.denied.subtitle")
              /// El paciente no tiene los permisos necesarios y se ha rechazado la llamada
              internal static let title = L10n.tr("LocalizableVC", "popup.professional.client.permission.audio.denied.title")
              internal enum Action {
                /// De acuerdo
                internal static let button = L10n.tr("LocalizableVC", "popup.professional.client.permission.audio.denied.action.button")
              }
            }
          }
          internal enum Video {
            internal enum Denied {
              /// Puedes recordarle que se los active utilizando el chat
              internal static let subtitle = L10n.tr("LocalizableVC", "popup.professional.client.permission.video.denied.subtitle")
              /// El paciente no tiene los permisos necesarios y se ha rechazado la videollamada
              internal static let title = L10n.tr("LocalizableVC", "popup.professional.client.permission.video.denied.title")
              internal enum Action {
                /// De acuerdo
                internal static let button = L10n.tr("LocalizableVC", "popup.professional.client.permission.video.denied.action.button")
              }
            }
          }
        }
      }
    }
  }

  internal enum Shared {
    /// Aceptar
    internal static let accept = L10n.tr("LocalizableVC", "shared.accept")
    /// Solicitud cancelada
    internal static let applicationCanceled = L10n.tr("LocalizableVC", "shared.applicationCanceled")
    /// 
    internal static let blank = L10n.tr("LocalizableVC", "shared.blank")
    /// LLAMANDO
    internal static let calling = L10n.tr("LocalizableVC", "shared.calling")
    /// Cámara y micro desactivados
    internal static let cameraAndMicrophoneDeactivated = L10n.tr("LocalizableVC", "shared.cameraAndMicrophoneDeactivated")
    /// Cámara desactivada
    internal static let cameraDeactivated = L10n.tr("LocalizableVC", "shared.cameraDeactivated")
    /// Cancelar
    internal static let cancel = L10n.tr("LocalizableVC", "shared.cancel")
    /// LLAMADA FINALIZADA
    internal static let endedCall = L10n.tr("LocalizableVC", "shared.endedCall")
    /// Error
    internal static let error = L10n.tr("LocalizableVC", "shared.Error")
    /// LLAMADA ENTRANTE
    internal static let incomingCall = L10n.tr("LocalizableVC", "shared.incomingCall")
    /// VIDEOLLAMADA ENTRANTE
    internal static let incomingVideoCall = L10n.tr("LocalizableVC", "shared.incomingVideoCall")
    /// % te está solicitando el cambio a videollamada
    internal static let isAskingYouChangeToVideoall = L10n.tr("LocalizableVC", "shared.isAskingYouChangeToVideoall")
    /// Micro desactivado
    internal static let microphoneDesactivated = L10n.tr("LocalizableVC", "shared.microphoneDesactivated")
    /// Ok
    internal static let ok = L10n.tr("LocalizableVC", "shared.Ok")
    /// Volviendo a conectar
    internal static let reconnecting = L10n.tr("LocalizableVC", "shared.reconnecting")
    /// Rechazar
    internal static let refuse = L10n.tr("LocalizableVC", "shared.refuse")
    /// LLAMADA RECHAZADA
    internal static let rejectedCall = L10n.tr("LocalizableVC", "shared.rejectedCall")
    /// VIDEOLLAMADA RECHAZADA
    internal static let rejectedVideoCall = L10n.tr("LocalizableVC", "shared.rejectedVideoCall")
    /// Solicitando el cambio a videollamada
    internal static let requestingChangeToVideoCall = L10n.tr("LocalizableVC", "shared.requestingChangeToVideoCall")
    /// Altavoz activado
    internal static let speakerActivated = L10n.tr("LocalizableVC", "shared.speakerActivated")
    /// VIDEOLLAMANDO
    internal static let videoCalling = L10n.tr("LocalizableVC", "shared.videoCalling")
  }

  internal enum Videocall {
    internal enum Rating {
      /// Buena
      internal static let good = L10n.tr("LocalizableVC", "videocall.rating.good")
      /// Mejorable
      internal static let improvable = L10n.tr("LocalizableVC", "videocall.rating.improvable")
      /// ¿Cómo ha sido la calidad de la llamada?
      internal static let message = L10n.tr("LocalizableVC", "videocall.rating.message")
      internal enum Call {
        /// Llamada finalizada
        internal static let ended = L10n.tr("LocalizableVC", "videocall.rating.call.ended")
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
