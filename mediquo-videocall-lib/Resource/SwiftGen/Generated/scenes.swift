// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum BusyVC: StoryboardType {
    internal static let storyboardName = "BusyVC"

    internal static let initialScene = InitialSceneType<mediquo_videocall_lib.BusyPopupViewController>(storyboard: BusyVC.self)

    internal static let busy = SceneType<mediquo_videocall_lib.BusyPopupViewController>(storyboard: BusyVC.self, identifier: "Busy")
  }
  internal enum CreateReportVC: StoryboardType {
    internal static let storyboardName = "CreateReportVC"

    internal static let initialScene = InitialSceneType<mediquo_videocall_lib.CreateReportViewController>(storyboard: CreateReportVC.self)

    internal static let createReport = SceneType<mediquo_videocall_lib.CreateReportViewController>(storyboard: CreateReportVC.self, identifier: "CreateReport")
  }
  internal enum MainVC: StoryboardType {
    internal static let storyboardName = "MainVC"

    internal static let initialScene = InitialSceneType<mediquo_videocall_lib.MainViewController>(storyboard: MainVC.self)

    internal static let main = SceneType<mediquo_videocall_lib.MainViewController>(storyboard: MainVC.self, identifier: "Main")
  }
  internal enum PopupVC: StoryboardType {
    internal static let storyboardName = "PopupVC"

    internal static let initialScene = InitialSceneType<mediquo_videocall_lib.PopupManagerViewController>(storyboard: PopupVC.self)

    internal static let popup = SceneType<mediquo_videocall_lib.PopupManagerViewController>(storyboard: PopupVC.self, identifier: "Popup")
  }
  internal enum RatingVC: StoryboardType {
    internal static let storyboardName = "RatingVC"

    internal static let initialScene = InitialSceneType<mediquo_videocall_lib.RatingViewController>(storyboard: RatingVC.self)

    internal static let rating = SceneType<mediquo_videocall_lib.RatingViewController>(storyboard: RatingVC.self, identifier: "Rating")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
