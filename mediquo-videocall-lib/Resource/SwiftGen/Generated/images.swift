// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let acceptCall = ImageAsset(name: "acceptCall")
  internal static let acceptCallOrVideoCall = ImageAsset(name: "acceptCallOrVideoCall")
  internal static let cameraOffIconStack = ImageAsset(name: "cameraOffIconStack")
  internal static let cameraOnIcon = ImageAsset(name: "cameraOnIcon")
  internal static let cameraOnIconStack = ImageAsset(name: "cameraOnIconStack")
  internal static let microphoneOffIconStack = ImageAsset(name: "microphoneOffIconStack")
  internal static let microphoneOnIconStack = ImageAsset(name: "microphoneOnIconStack")
  internal static let phoneOnIcon = ImageAsset(name: "phoneOnIcon")
  internal static let rejectCallOrVideoCall = ImageAsset(name: "rejectCallOrVideoCall")
  internal static let reverseCameraOffIconStack = ImageAsset(name: "reverseCameraOffIconStack")
  internal static let reverseCameraOnconStack = ImageAsset(name: "reverseCameraOnconStack")
  internal static let speakerOffIconStack = ImageAsset(name: "speakerOffIconStack")
  internal static let speakerOnIconStack = ImageAsset(name: "speakerOnIconStack")
  internal static let toastSatusCameraOffIcon = ImageAsset(name: "toastSatusCameraOffIcon")
  internal static let toastSatusMicrophoneOffIcon = ImageAsset(name: "toastSatusMicrophoneOffIcon")
  internal static let toastStatusReconnectingIcon = ImageAsset(name: "toastStatusReconnectingIcon")
  internal static let toastStatusSpeakerOffIcon = ImageAsset(name: "toastStatusSpeakerOffIcon")
  internal static let decline = ImageAsset(name: "decline")
  internal static let icClose = ImageAsset(name: "icClose")
  internal static let imgCallOff = ImageAsset(name: "imgCallOff")
  internal static let imgVideocallOff = ImageAsset(name: "imgVideocallOff")
  internal static let requestingChangeToVideoCallAccept = ImageAsset(name: "requestingChangeToVideoCallAccept")
  internal static let requestingChangeToVideoCallReject = ImageAsset(name: "requestingChangeToVideoCallReject")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
