//
//  mediquo_videocall_lib.h
//  mediquo-videocall-lib
//
//  Created by David Martin on 21/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for mediquo_videocall_lib.
FOUNDATION_EXPORT double mediquo_videocall_libVersionNumber;

//! Project version string for mediquo_videocall_lib.
FOUNDATION_EXPORT const unsigned char mediquo_videocall_libVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <mediquo_videocall_lib/PublicHeader.h>


