//
//  ViewController.swift
//  mediquo-videocall-lib-demo
//
//  Created by David Martin on 21/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit
import mediquo_videocall_lib

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // swiftlint:disable:next line_length
        VideoCall.initLib(to: .production, with: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwaS5tZWRpcXVvLmNvbS9hcGkvYXV0aC9yZWZyZXNoIiwiaWF0IjoxNTg4OTIwMDE4LCJuYmYiOjE1ODg5MjAwMTgsImp0aSI6IlY2cEJPM0N1T01jVjcxdGsiLCJzdWIiOiJhNDcyNDNmMy05YTBlLTRjY2ItYWNlYS1lNGFiM2MzMDNjNGYiLCJwcnYiOiI3ZmQ4ZDc2NzY4ZTE3ZWI1MWJmZjU2NjYyYjc2N2I4MWY4Y2RkMzY1IiwibWV0YSI6eyJ0eXBlIjoiY3VzdG9tZXIiLCJuYW1lIjoiRGF2aWQiLCJnZW5kZXIiOjAsImJpcnRoX2RhdGUiOiIxOTgzLTEyLTEwIiwiY291bnRyeV9jb2RlIjoiZXMiLCJsYW5ndWFnZV9jb2RlIjoiZXMifX0.sLjhN7SE16ysK5GpeLeeWjAXKaeB603nTwLsPO2Qdj4")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let controller = try? VideoCall.getVideoCallManager().showIncomingCall(by: .patient,
                                                                                     by: VideoCallNotificationSchema(call: CallModel(uuid: "wfwef", roomId: 1, type: .video, sessionId: nil, token: nil))) else {
            return
        }
        self.present(controller, animated: true, completion: nil)
    }
}
