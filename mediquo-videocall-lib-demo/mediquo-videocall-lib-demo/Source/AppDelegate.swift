//
//  AppDelegate.swift
//  mediquo-videocall-lib-demo
//
//  Created by David Martin on 21/04/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
